---
title: Count k-mers in a sequence
---

<div id="menu_md"> 

## Table of Contents

<div class="toc_md"> 

- [Introduction](#intro)
- [Functional solution](#functional)
  - [Listing all k-mers](#list)
  - [Counting repetitions](#count)
  - [Printing solution](#print)
- [Using Hashtable](#hashtable)
</div>
</div>

<div id="tuto"> 

# Count k-mers in a sequence

## Introduction<a id="intro"></a>

<div style="text-align: justify"> 
This tutorial aims at learning how to find recursively all unique k-mers in a sequence and how many times they occur, using OCaml.
The list of k-mers represent all the substring of size k in a given sequence. This could be useful when trying to assemble genome using De Bruijn graphs.<br/>
<br/>
By the end of this tutorial we will be able to call functions giving k, a sequence and an empty list as arguments like the following example, where k=3 and seq="ACGAAATAAAACGTAA". 
</div>

<!-- $MDX skip -->
```ocaml
let list = list_kmer 3 "ACGAAATAAAACGTAA" in
print_pairs (count_uniq (List.sort_uniq String.compare list) list)
|> print_string;;
```
<div style="text-align: justify"> 
In imperative programming, to list all substring, we would loop on the sequence, then, wether the k-mer has already been encounter, create or update the number of repetition in a dictionnary.

An example in Python could be :

</div>

```python
def count_kmer (k, seq):
    dict_kmer = {} # Creating the dictionnary taking k-mers as keys and counts as values
    for i in range(len(seq)-k+1): 
        kmer = seq[i:i+k] # Creating the k-mer at position i
        if kmer in dict_kmer: # Checking if the key exist
            dict_kmer[kmer] += 1 
        else:
            dict_kmer[kmer] = 1
    
    return dict_kmer


my_dict  = count_kmer(3, "ACGAAATAAAACGTAA")

for km in my_dict:
    print(f"{km} - {dict[km]}")
```

<div style="text-align: justify"> 
In functional programming, variables are immutable and instead of looping we recursively call functions, therefore we can't have the same approach. So in order to solve this problem, this tutorial offers to go through three steps:<br/>

- Listing all k-mers, given k (integer) and a sequence (string)
- Defining unique k-mers and counting their repetition, from the list of k-mers
- Printing the wanted information in a prompt

Then it will go through a solution using hashtable.

</div>

# Functional Solution (without hashtable)<a id="functional"></a>

## Listing all k-mers <a id="list"></a>
<div style="text-align: justify"> 

So first we want to create a recursive function which will take as arguments an integer (k) and a string (sequence) and will return a list of all substring of size k of the sequence.
</div>

<!-- $MDX skip -->
```ocaml
let rec list_kmer k sequence =
```
<div style="text-align: justify"> 
When implementing a recursive function, an important thing to think about is the stopping condition. There may be several, and they are conditions that won't call the function recursively, but stop it. 

For more information, go to [Recursive function](./recursivefunc.md).

Here, the recursion will be applied on the sequence, so the stopping condition would be :
* The sequence is smaller than k.

To do that in Ocaml, we can use the [pattern matching](./recursivefunc.md#implement). Here we'll do the match on the length of the sequence.

* The length of the sequence is smaller than k = return the full list built as <code>acc</code> reversed
* Any other length (symbolize by the '_' caracter) = appends the first substring of the sequence to the recursive call of the rest of the sequence.
</div> 

```ocaml
# let rec list_kmer_aux k seq acc=
  let len = (String.length seq) in
    match len with 
    |n when n < k -> List.rev acc
    |_ -> list_kmer_aux k (String.sub seq 1 (len-1)) ((String.sub seq 0 k) :: acc);;
val list_kmer_aux : int -> string -> string list -> string list = <fun>
```
<div style="text-align: justify">
In order to make it more efficient and store less strings, we can keep the full sequence and add an argument for the position of k-mers instead of creating substring. With this method the argument <code>seq</code> stays the same at every call and the argument <code>pos</code> increase by one.

</div> 

```ocaml
# let rec list_kmer_aux k seq acc pos=
  let len = (String.length seq) in
  match pos with 
      |n when n > len-k -> List.rev acc
      |_ ->list_kmer_aux k seq ((String.sub seq pos k) :: acc) (pos+1);;
val list_kmer_aux : int -> string -> string list -> int -> string list =
  <fun>
```

<div style="text-align: justify"> 
As the function <code>list_kmer_aux</code> uses arguments such as <code>acc</code> and <code>pos</code> which must always have the same value at first, the final step would be to create a function <code>list_kmer</code> calling the auxiliary function to make sure the user uses the right values and doesn't change it. 
</div>

```ocaml
# let list_kmer k seq=
  list_kmer_aux k seq [] 0;;
val list_kmer : int -> string -> string list = <fun>
```

### Note
In the last step of the pattern matching, an other way you could call the recursion would be :
<!-- $MDX skip -->
```ocaml
|_ -> String.sub seq pos k :: list_kmer k seq (pos+1)
```
<div style="text-align: justify"> 
This appends the recursive call to the begining of a list, which works and is not wrong. However, it is not the most efficient way if the number of recursion accumulates. That's why in the example we used <a>terminal recursion</a>, which makes sure the recursion is done last.<br/>
</div>


## Counting repetitions <a id="count"></a>
<div style="text-align: justify"> 
We now want to implemente a recursive function taking as arguments the list of k-mers we just created as well as the same list with unique values and will return a list of pairs (string * integer).<br/>
In OCaml, there is function <code>List.sort_uniq</code> sortting a list in increasing order according to a comparison function and removing all duplicates.
</div>

<!-- $MDX skip -->
```ocaml
List.sort_uniq String.compare list
```

<div style="text-align: justify"> 
The function takes two arguments, a comparision function, here `String.compare` and a list.

As we are working on list, pattern matching is simple (see [Recursive function](./recursivefunc.md) for more informations).
</div>

```ocaml
# let rec count_uniq_aux uniq full acc =
  match uniq with
  |[]->List.rev acc
  |h::t -> count_uniq_aux t full ((h, List.length (List.filter (fun x -> x=h) full)) :: acc);;
val count_uniq_aux :
  string list -> string list -> (string * int) list -> (string * int) list =
  <fun>
```

<div style="text-align: justify"> 
Just like for the function <code>list_kmer_aux</code>, we need to create a function <code>count_uniq</code>, which will call the function <code>count_uniq_aux</code> to specify the value of <code>acc</code> and to not let the user modify it.
</div>

```ocaml
# let count_uniq uniq full=
  count_uniq_aux uniq full [];;
val count_uniq : string list -> string list -> (string * int) list = <fun>
```

### Note

<div style="text-align: justify"> 

The functions <code>count_uniq_aux</code> and <code>count_uniq</code> are general functions that could count for any type, this is why they take list of a' types.

</div>

## Printing information <a id="print"></a>
<div style="text-align: justify"> 
Finally, we will try to implemente a recursive function, printing the informations in an understable way. This step is actually optional as it depends on what you wish to do with those informations. 
An example of display could be:
</div>


```
AAA - 3                                                                   
```

We can use the same method as before for patern matching and easily print each pair of the list, using `string_of_int` to transform integer in string.<br/>

```ocaml
# let rec print_pairs list = (* Recursive function printing pairs of k-mer/iteration *)
  match list with (* Stopping conditions : empty list *)
  |[]->""
  |(h,n)::t->h^" - "^(string_of_int n)^"\n"^(print_pairs t);;
val print_pairs : (string * int) list -> string = <fun>
```

# Using Hashtable<a id="hashtable"></a>

<div style="text-align: justify"> 

All the previous steps guided you to build fully functional functions in order to count k-mers. But this method is less efficient then the imperative one, because instead of using dictionnary, we use lists, one to list all k-mers and another one to sort all uniq k-mers and count them.
Despite being a functional language, OCaml can use hashtable, which will be a lot more efficient. For more information, please read [hashtable](./hashtable.md).

<code>list_kmer_aux</code> returns the hashtable if the value of the position is above the total lenght of the sequence minus the length of the k-mer, meaning we can't form any more words of size k. Else, it checks if the k-mer is already a key in the hashtable and create a variable <code>count</code>, adding 1 to the existing value of the hashtable or taking 1 as a value to create it after.
We then update the value corresponding to the k-mer in the hashtable.
</div>

```ocaml
# let rec list_kmer_aux k seq my_hash pos len=
    if pos > len - k then
      my_hash
    else 
      let kmer = String.sub seq pos k in 
      let count = match Hashtbl.find_opt my_hash kmer with
      | None -> 1
      | Some x -> x + 1
      in
      Hashtbl.replace my_hash kmer count ;
      list_kmer_aux k seq my_hash (pos + 1) len
  ;;
val list_kmer_aux :
  int ->
  string -> (string, int) Hashtbl.t -> int -> int -> (string, int) Hashtbl.t =
  <fun>
```

<div style="text-align: justify">

We then create <code>list_kmer</code>, a function calling <code>list_kmer_aux</code> to set default values of <code>my_hash</code>, <code>pos</code> and <code>len</code>.

</div>

```ocaml
# let rec list_kmer k seq=
  let len = String.length seq in
  let my_hash = Hashtbl.create (len - k + 1) in
  list_kmer_aux k seq my_hash 0 len;;
val list_kmer : int -> string -> (string, int) Hashtbl.t = <fun>
```

<div style="text-align: justify"> 

<code>print_iter</code> is an optional function to implement but which allows us to print a pair of string and integer with the wanted form.

</div>

```ocaml
# let print_iter k v =
  print_endline (k ^" - "^(string_of_int v));;
val print_iter : string -> int -> unit = <fun>
```

<div style="text-align: justify"> 

<code>print_hashtbl</code> finally calls the function <code>print_iter</code> on each pair of key/value of the hashtable.

</div>

```ocaml
# let print_hashtbl my_hash=
    Hashtbl.iter print_iter my_hash;;
val print_hashtbl : (string, int) Hashtbl.t -> unit = <fun>
```

<div style="text-align: justify"> 

The final command and its output would then be :

</div>

```ocaml
# print_hashtbl (list_kmer 3 "ACGAAATAAAACGTAA" );;
ATA - 1
AAC - 1
CGA - 1
CGT - 1
AAA - 3
GAA - 1
AAT - 1
GTA - 1
ACG - 2
TAA - 2
- : unit = ()
```

<div style="text-align: justify"> 

Now that we know how to count k-mers with string types, to go further, we can try and do it with dna type we implemented [here](./type.md).

</div>

</div>
