---
title: Hashtable in OCaml
---

<div id="menu_md"> 

## Table of Contents

<div class="toc_md"> 

- [Introduction](#intro)
- [Some Hashtable Methods](#methods)
</div>
</div>

<div id="tuto">

# Hashtable in OCaml

## Introduction

<div style="text-align: justify"> 

In Informatics, hashtables are an imperative way to store data. It is a structure data allowing key/value association ie. a non-ordered array. Instead of values being indexed through integers, hashtable's value can be accessed by its key, transformed by hash function into a hash value.

<div style="text-align: center">

![Example of Hashtable affectation](images/hashtable.svg)

</div>

Despite OCaml being a functional programming where variables are supposed to be immutable, it also is a languege offering way to do imperative programming if needed sush as using loops are hashtables.
To understand and illustrate how hashtables work in OCaml we will take the example of a hashtable associating proteins' id to the length of those proteins in amino acids.
</div>

```ocaml
# let prot_len = [("P17252", 670); ("P06213", 1382); ("P24941", 298)];;
val prot_len : (string * int) list =                                               +  [("P17252", 670); ("P06213", 1382); ("P24941", 298)]
```

## Some Hashtable Methods

<div style="text-align: justify">

First to create a hashtable, we can use the method <code>Hashtabl.create</code> giving as argument the initial size of the hashtable which should be an intial guess of the expected number of elements.

</div>

```ocaml
# let my_hash = Hashtbl.create 10;;
val my_hash : ('_weak1, '_weak2) Hashtbl.t = <abstr>
```

<div style="text-align: justify">

To find the value of a key, we can either use <code>Hashtabl.find</code> or <code>Hashtabl.find_opt</code> in the first case, if the key doesn't exists, an error is raised, while the second method returns an option type, meaning it will return the value if it exists or <code>None</code> if it doesn't. Both functions take as arguments the name of the hashtable and the key we want to find.

</div>

```ocaml
# Hashtbl.find_opt my_hash "P17252";;
- : '_weak3 option = None
```

<div style="text-align: justify">

To replace the value of an existing key, we can us <code>Hashtabl.replace</code>, taking as argument the key needed and the updated value. If the key is not in the hashtable, the pair key/value is added.

</div>

```ocaml
# Hashtbl.replace my_hash "P17252" 672;;
- : unit = ()
```

<div style="text-align: justify">

The method <code>Hashtabl.iter</code> takes as arguments a function that will be applied on each pair of key/value of an hashtable, given as the second argument. The function used must take as a key and a value. In the following example, we create a function <code>print_iter</code> printing as a string, a pair of key/value and used by <code>Hashtabl.iter</code>, in the function <code>print_hash</code>.

</div>

```ocaml
# let print_iter k v =
  print_endline (k ^" - "^(string_of_int v));;
val print_iter : string -> int -> unit = <fun>
```

```ocaml
# let print_hashtbl my_hash=
    Hashtbl.iter print_iter my_hash;;
val print_hashtbl : (string, int) Hashtbl.t -> unit = <fun>
```

<div style="text-align: justify">

Here are some example of Hashtable usage :

* [Count k-mers in a sequence](./countkmer.md)
* [Transcript and translate a sequence](./tra.md)

</div>

</div>
