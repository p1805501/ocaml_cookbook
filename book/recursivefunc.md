---
title: A first approach to recursive functions
---

<div id="menu_md"> 

## Table of Contents

<div class="toc_md"> 

- [How to implement a recursive function in OCaml](#implement)
- [Terminal recursion](#terminal)
</div>
</div>

<div id="tuto"> 

# A first approach to recursive functions

<div style="text-align: justify">
A recursive function is a function calling itself, so the result depends on that call.<br><br>
Let's take an example and say we want to find the minimum of a list of temperatures (integers). The aim would be to find the solution for a sub-list and compare it to the first element. So the comparison would be between the first integer of the list and the minimum of the rest.
</div>

<div style="text-align: center">

![An illustration of how recursivity works](images/Recursivefunc.svg)
</div>

## How to implement a recursive function in OCaml <a id="implement"></a>


<div style="text-align: justify">
To use a recursive function in OCaml, the key word <code> rec </code> must be added when defining a function. We then need to find stopping conditions that will make sure the recursive function knows what to do when given specific arguments. If the argument is an integer the stopping condition could be 0 and/or 1. If we go back to the previous example, when looking for the minimum of a list, we need the function to stop in two cases:

- If the list is empty
- If the list has only one element

To do that in OCaml, we can use a control flow statement, called [pattern matching](./recursivefunc.md#implement). In this case, we need to raise an error if the list is empty because we can't return a default value of integer. While when there is only one element in the list, we want to return this element. In any other case, the function will take the minimum between the first element and the minimum of the rest of the list, just like in the illustration above.
</div>

```ocaml
# exception ValueError;;
exception ValueError

# let rec minimum_list int_list=
   match int_list with 
    |[] -> raise ValueError
    |h::[] -> h
    |h::t-> min h (minimum_list t);;
val minimum_list : 'a list -> 'a = <fun>
```

```ocaml
# minimum_list [3; 6; 12; -2; 0; 7];;
- : int = -2
```
## Terminal recursion <a id="terminal"></a>
<div style="text-align: justify">
An other more efficient way to use recursion is to do terminal recursion. This makes sure the recursion call is done last and saves memory space, because only the address of the calling function, needs to be saved on the execution stack.

A way to do that, is to add the temporary solution we want to return as an argument. To make sure the default value won't affect the result we create an auxiliary recursive function which will be called in the function <code>minimum_list</code> where the default value <code>mini</code> will be defined
</div>

<!-- $MDX skip -->
```ocaml
let rec minimum_list_aux int_list mini=
    match int_list with 
    |[] -> raise ValueError
    |h::[] -> min mini h
    |h::t-> minimum_list_aux t (min h mini);;

let rec minimum_list int_list=
  match int_list with 
  |[] -> raise ValueError
  |h::t -> minimum_list_aux int_list h;;
```

```ocaml
# minimum_list [3; 6; 12; -2; 0; 7];;
- : int = -2
```


