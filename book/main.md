# Ocaml Cookbook

This cookbook aims at helping beginners in functionnal programing by introducing them solutions to simple solutions to bioinformatical problems.

<div style="text-align: justify"> 
In functionnal programming, variables are immutable and instead of looping we recursively call functions, therefore we can't have the same approach. So in order to solve this problem, this tutorial offers to go through three steps:<br><br>
</div>

[Here](countkmer.md) you will learn how to count kmers
[Here](countkmer.md), you will learn how to display DNA sequences.


<u>Note :</u> In Ocaml, the convention is to name the conversion functions <code>output_type_of_input_type</code>. 

```
Common way to name conversion functions :
string_of_int i
int_to_string i
```

The advantage over <code>input_type_to_output_type</code> is that the input type directly precedes the parameter of that type which adds clarity.

```
list_of_file f
looks like list of (file f)
```
