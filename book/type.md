---
title: Represent a DNA sequence with a type
--- 

<div id="menu_md"> 

## Table of Contents

<div class="toc_md"> 

- [Introduction](#intro)
- [Creating a type](#type)
- [Creating functions](#functions)
- [An example of function using the dna type](#function)
- [Checking the entire sequence](#sequence)
- [Printing solution](#print)

</div>
</div>

<div id="tuto"> 

# Represent a DNA sequence with a type

## Introduction <a id="intro"></a>

<div style="text-align: justify"> 

Type creation is a really important concept of OCaml which is a typed inferred language.</br>
It allows the user to create functions using this type without needing to check every characteristics of the type for every function.

In this tutorial, we will learn how to create a custom type as well as the functions needed to use it. Finally, we will create a function using this type to see how it works and how it can simplify the coding process.
</div> 

## Creating types : the nucleotid and dna type <a id="type"></a>

<div style="text-align: justify"> 

Let's take as an example the case where we want to manipulate DNA sequences.</br>
For example, if we want to create the reverse complement of a dna sequence, we could create a function which will take our custom type, <code>dna</code> as an argument and return a <code>dna</code>.</br>
Thus, with the definition of our type, every function that use it would automatically reject non-DNA type as these are not compatibles with our function, without us needing to worry about checking the type of our argument everytime.

So the first step is to define our types : 

- the <code>nucleotide</code> type which represent a single character :
  - Here we just define it as being A or C or G or T. As these are not character nor strings we don't need to add quotes.
</div> 

```ocaml
# type nucleotide = A | C | G | T ;;
type nucleotide = A | C | G | T
```

<div style="text-align: justify"> 

- the <code>dna</code> type which represent a list of nucleotides : 
  - Here we define it with the constructor <code>Sequence</code> (constructors always starts with a capital letter) which takes a nucleotid list as an argument to return a <code>dna</code>.
</div> 

```ocaml
# type dna = Sequence of nucleotide list;;
type dna = Sequence of nucleotide list
```

<div style="text-align: justify"> 

Thus our new types would look like this :
</div> 

```ocaml
# let nuc = A;;
val nuc : nucleotide = A

# let dna_sequence = Sequence [A;C;G;T];;
val dna_sequence : dna = Sequence [A; C; G; T]
```

<div style="text-align: justify"> 

As we can see, after defining our types, the interpreter recognize them automatically.
</div> 

## Creating conversion functions <a id="functions"></a>

<div style="text-align: justify"> 

The second step in creating our type, is to define functions to convert native types of OCaml like <code>String</code>, <code>Char</code> . . . .</br>
In our case we can define the following functions :

- convert a character into a nucleotide :

  - Here, we use a match to attribute the right nucleotide values. If the character doesn't correspond with a nucleotide, an error occurs, indicating to the user that the character provided to the function is not a nucleotide.
</div> 

```ocaml
# let nuc_of_char character =
    match Char.uppercase_ascii character with
    | 'A' -> A
    | 'C' -> C
    | 'G' -> G
    | 'T' -> T
    | _ -> failwith ("This character is not a nucleotide");;
val nuc_of_char : char -> nucleotide = <fun>
```

<div style="text-align: justify"> 

- convert a string into a type dna :

  - Here, we have to define a recursive function which will take a string as an argument and call the <code>nuc_of_char</code> function on every character, to create a nucleotide list,
  - Then, we apply our constructor <code>Sequence</code> to our newly created nucleotid list.
</div> 

```ocaml
# let dna_of_string seq = 
    let rec nuclist_of_string ?(acc=[]) sequence =
      match String.length sequence with
      | 0 -> acc
      | _ -> nuclist_of_string ~acc:((nuc_of_char (sequence.[0])) :: acc) (String.sub sequence 1 ((String.length sequence)-1))
    in
    let nuc_list = List.rev (nuclist_of_string seq) in
    Sequence nuc_list;;
val dna_of_string : string -> dna = <fun>
```

<div style="text-align: justify"> 

We can also define the reverse operations that would be necessary for display purposes (as the functions used for display only takes specific types, for example <code>print_string</code> . . . ) :

- First, we define a recursive function that convert a <code>dna</code> in a <code>String</code>
  - Here we need to pass a <code>dna</code> type to the function, to do so, we have to call the constructor Sequence to ensure the type of the argument.
</div> 

```ocaml
# let rec string_of_dna ?(acc="") (Sequence seq) =
    match seq with
    | [] -> acc
    | h::t -> let character = 
      match h with 
      | A -> "A"
      | C -> "C"
      | G -> "G"
      | T -> "T"
      in string_of_dna ~acc:(acc^character) (Sequence t);;
val string_of_dna : ?acc:string -> dna -> string = <fun>
```

<div style="text-align: justify"> 

- Then we create the function <code>print_dna</code> that will call <code>print_endline</code> on the string returned by <code>string_of_dna</code>.
</div> 

```ocaml
# let print_dna (Sequence seq) =
    let dna_string = string_of_dna ~acc:"" (Sequence seq) in
    print_endline dna_string;;
val print_dna : dna -> unit = <fun>
```

## An example of function using the <code>dna</code> type <a id="function"></a>

<div style="text-align: justify"> 

With the definition of our types, we can now create functions that will take dna type variables as an argument.

For example, we could create a function to create the reverse complement of a dna sequence

Here, we define a terminal recursive function (see more [here](recursivefunc.html)) that will, for each nucleotide, return the complementary nucleotide and create a nucleotide list.</br>
Then, we have to reverse the list, as it will be in incorrect order and apply our constructor <code>Sequence</code> to it.
</div> 

```ocaml
# let reverse_complement (Sequence dna_seq) =
    let rec complement ?(acc=[]) dna_seq =
      match dna_seq with
      | [] -> acc
      | h::t -> let nucleotide =
        match h with 
        | C -> G 
        | G -> C
        | A -> T 
        | T -> A
      in
      complement ~acc:(nucleotide::acc) t
    in
    Sequence (List.rev (complement dna_seq));;
val reverse_complement : dna -> dna = <fun>
```

## An example of application of the function <a id="function"></a>

<div style="text-align: justify"> 

Finally, if we apply this function we have :
</div> 

```ocaml
# let dna_string = "ACGAAATAAAACGTAA";;
val dna_string : string = "ACGAAATAAAACGTAA"

# let dna_sequence = dna_of_string dna_string;;
val dna_sequence : dna =
  Sequence [A; C; G; A; A; A; T; A; A; A; A; C; G; T; A; A]

# reverse_complement dna_sequence;;
- : dna = Sequence [T; G; C; T; T; T; A; T; T; T; T; G; C; A; T; T]
```

<div style="text-align: justify"> 

Now that you know how to create a type, you can try to implement one for the functions presented in the rest of the cookbook.
</div>

</div>
