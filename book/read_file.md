---
title: Read a file in Ocaml
---

<div id="menu_md"> 

## Table of content

<div class="toc_md"> 

- [Read the file as a string](#string)
- [Read the file as a list of string](#list)
- [Factor the opening and closing of files](#factor)
- [Conclusion](#conclusion)

</div>
</div>

<div id="tuto"> 

# Read a file in Ocaml

## Read the file as a string<a id="string"></a>

<div style="text-align: justify"> 
Learning to read a file is an important step in learning any programming language. Here is a step-by-step guide to reading a file.
</div>

<!-- $MDX skip -->
```ocaml
let f = open_in path;;
```

<div style="text-align: justify">

The **open_in** function allows to link a file via a path and a variable. Here, our variable **f** represents our file. Its type is "in_channel".
In fact, the variable represents a reading channel and a "cursor" that is placed at the beginning of the file. When we use functions to browse the file, the "cursor" will move.

For example, to read a line from a file, we use the **input_line** function which moves the cursor one line forward and moves the cursor to the beginning of the next line.
</div>

<div style="text-align: center">

![](images/read_file.svg)
</div>

<div style="text-align: justify">

When the **input_line** function tries to read a line but there is no more, it returns an "End_of_file" error. To prevent this error from interrupting the program, it must be captured by the **try** control structure. As soon as an error is raised in a **try** block, the **with** block corresponding to the raised error is executed. In our case, we will just use it to close the reading channel with the **close_in** function.
</div>

```ocaml
# let rec read_line f = ( (* factorisez open/close file rwocaml+*)
  try
      let line = input_line f in 
      line ^ (read_line f)
  with End_of_file -> let () = close_in f in ""
  );;
val read_line : in_channel -> string = <fun>
```
<div style="text-align: justify">

Here, we simply concatenate each line into a huge string. If we turn it into a function :
</div>

```ocaml
# let string_of_file ?(interline="\n") path = (
    let f = open_in path in  
    let rec read_line ?(already_read="") f = (
      try
        let line = input_line f in 
        if already_read = "" then read_line ~already_read:(line) f 
        else read_line ~already_read:(already_read ^ interline ^ line) f
      with End_of_file -> let () = close_in f in already_read
    ) in 
    read_line f
  );;
val string_of_file : ?interline:string -> string -> string = <fun>
```

<div style="text-align: justify">

Here, the function takes the optional argument **interline** to be able to specify the character between each line (by default **'\n'** or return to line) because input_line does not read the '\n' of end of line. But you can put something else.
The optional argument **already_read** allows it to do terminal recursion. The last instruction of the function must be the recursive call, and for that the information brought by the function must be passed in one of its own parameters.
</div>

## Read the file as a list of string<a id="list"></a>

<div style="text-align: justify">
We can modify our basic function to render a list of strings, each element being one of the lines in the file.
</div>

```ocaml
# let list_of_file path = (
    let f = open_in path in  
    let rec read_line ?(already_read=[]) f = (
      try
        let line = input_line f in 
        read_line ~already_read:(line::already_read) f
      with End_of_file -> let () = close_in f in List.rev already_read
    ) in 
    read_line f
  );;
val list_of_file : string -> string list = <fun>
```

<div style="text-align: justify">

First of all, the optional <code>interline</code> parameter is no longer necessary because we no longer concatenate strings. Moreover, we don't need to check if it's the first line we read (by looking if <code>already_read=""</code>) because the recursive call will be the same in both cases thanks to the <code>::</code> operator.
<div>

## Factor the opening and closing of files <a id="factor"></a>

<div style="text-align: justify">

Opening a file creates a "channel", but these channels require resources from the machine and it is a good practice to close them each time you finish working on a file. However, it happens that an error occurs in the heart of one of our algorithms and that the program stops without closing the channel. So we will build a function that takes a file and another function as parameters, giving an open channel to the latter and closing the file, allowing to catch an error if it occurs and returning it after closing the file.

```ocaml
# let with_file path func = ( (* 1 *)
    let channel = open_in path in (* 2 *)
    try
      let func_result = func channel in (* 3 *) 
      let () = close_in channel in (* 4 *)
      func_result (* 5 *)
    with e -> let () = close_in channel in raise e (* 6 *)
  );;
val with_file : string -> (in_channel -> 'a) -> 'a = <fun>
```

- (1) : Our function takes two arguments: the file path ( string ) and a function 
- (2) : Opening the file : channel is our <code>in_channel</code>
- (3) : Evaluation of the function
- (4) : File closure
- (5) : Return the result of the function
- (6) : In case of an error, the file is closed and the error is returned. Since raise returns an <code>'a</code> type, the compiler is not bothered and considers that it is the same type of return as the <code>func</code> function and considers our <code>with_file</code> function correct because it doesn't matter if there is an error or not, the result keep the same type.

<u>Reminder :</u> The <code>'a</code> type is a notation to signify "any type". However, if several functions return a type <code>'a</code>, then this must be consistent and this type must be the same everywhere (we will use <code>'b</code>, <code>'c</code>, etc to designate types that may be different. Here, the <code>raise</code> function adapts to the type returned by <code>func</code>.
</div>

<u>Note :</u> Some conditions must be met for <code>with_file</code> to work. First, <code>func</code> must have its arguments already applied. For example, a <code>read_n_line</code> function of type <code>int -> in_channel -> string</code> list must have an integer already applied when passed as a parameter to <code>with_file</code>. Moreover, the parameter of type <code>in_channel</code> must be in the last position. Finally, the <code>func</code> function will still have to handle exceptions that suit it, such as the <code>End_of_file</code> exception that shows the end of the file. Otherwise, it will be caught by the <code>with_file</code> function and the exception will interrupt the program.

Our previous function list_of_file would look like this if we wanted to pass it to with_file :

```ocaml
# let rec list_of_file ?(already_read=[]) channel = (
    try
      let line = input_line channel in 
      list_of_file ~already_read:(line::already_read) channel
    with End_of_file ->   List.rev already_read
  );;
val list_of_file : ?already_read:string list -> in_channel -> string list =
  <fun>
```

## Conclusion <a id="conclusion"></a>

<div style="text-align: justify">

We have seen two ways to retrieve the content of a file by storing it in two different ways.
<div>

<!-- string_of_file? did it in tra.md why not explain it here? -->

</div>
