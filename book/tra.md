---
title: Transcript and translate a sequence
---
<!-- to make mdx work here we need to put the file codon_table.json in ocaml_cookbook/_build/default/book-->

<div id="menu_md"> 

## Table of content

<div class="toc_md"> 

- [Biological Problem](#bio)
- [Transcription](#trans)
  - [Imperative Python Style](#ips_trans)
  - [Imperative Ocaml Style](#ios_trans)
  - [Functional Ocaml Style](#fos_trans)
- [Translation](#tra)
  - [Get a codon table](#codon)
    - [Make the file as a list](#file-list)
    - [Convert the list as a hash table](#list-ht)
    - [Convert the list in a Map](#list-map)
  - [Doing the translation](#tra2)
    - [With hash table](#ht)
    - [With Map](#map)
    - [Main differences between Hashtbl and Map module for our code](#diff)
- [Conclusion](#conclusion)

</div>
</div>

<div id="tuto">

# Transcript and translate a sequence

## Biological problem <a id="bio"></a>

<div style="text-align: justify"> 

The two most common operations in biology done on DNA are the transcription and translation steps. In this sheet, we will see how to answer this problem in Ocaml.
</div>

<div style="text-align: center"> 

![schema tra](images/tra.svg)
</div>

<div style="text-align: justify"> 

In this file, we'll assume that a DNA Sequence is a string with only <code>A</code>, <code>C</code>, <code>G</code> or <code>T</code>. In the same way, a RNA Sequence have <code>A</code>, <code>C</code>, <code>G</code> or <code>U</code>.

</div>

## Transcription<a id="bio"></a>

<div style="text-align: justify"> 

During transcription, RNA polymerase takes each deoxyribonucleotide (<code>A</code>, <code>C</code>, <code>G</code> or <code>T</code>) from the DNA and transcribes them into ribonucleotides. From a bioinformatics point of view, this makes almost no difference to us, because ribonucleotides are very similar to deoxyribonucleotides and are called the same (<code>A</code>, <code>C</code> and <code>G</code>), except for thymine which becomes uracil. So we replace <code>T</code> by <code>U</code>.

</div>

<div style="text-align: center"> 

![schema transcription](images/transcription.svg)

</div>


### Imperative Python Style<a id="ips_trans"></a>

In Python, the code will looks like that : 

```python
>>> dna = "ACGCGCGAGGGTGA"
>>> dna = dna.replace("T","U")
>>> print(dna)
ACGCGCGAGGGUGA
```

The characters <code>T</code> became <code>U</code>. But in Ocaml, the standard library don't have function like <code>replace</code>.

We'll need to define a more precise function to understand the algorithm behind.

```python
>>> def transcription_nuc(nucleotide):
...     if nucleotide == 'T':
...         return 'U'
...     else:
...          return nucleotide

>>> def transcription(sequence):
...     transcript = ""
...     for char in sequence:
...         transcript += transcription_nuc(char)
...     return transcript

>>> print(transcription(dna))
ACGCGCGAGGGUGA
```

As we see, we need to define two function :
- <code>transcription_nuc</code> : take a nucleotide and return it or a <code>U</code> if the nucleotide is <code>T</code>,
- <code>transcription</code> : take a DNA sequence (as string) and return the associate RNA sequence.

### Imperative Ocaml Style <a id="ios_trans"></a>

By transforming our functions into Ocaml, we obtain the code below :

```ocaml
# let rec transcription_nuc nuc = (
  if Char.uppercase_ascii nuc = 'T' then
    'U'
  else 
    Char.uppercase_ascii nuc
  );;
val transcription_nuc : char -> char = <fun>

# let rec transcription sequence = ( 
    let transcript = ref "" in
    for i = 0 to String.length sequence - 1 do
        transcript := !transcript ^ Char.escaped (transcription_nuc sequence.[i]);
    done;
    !transcript
  );;
val transcription : string -> string = <fun>

# let dna = "ACGCGCGAGGGTGA";;
val dna : string = "ACGCGCGAGGGTGA"
# print_endline (transcription dna);;
ACGCGCGAGGGUGA
- : unit = ()
```

<div style="text-align: justify"> 
Here is what the code in Ocaml would look like if we tried to reproduce the code in Python. However, this is not the way of coding encouraged by a functional language.
</div>

### Functional Ocaml Style<a id="fos_trans"></a>

```ocaml
# let transcription_nuc nuc = (
    match Char.uppercase_ascii nuc with
    | 'T' -> 'U'
    | _ -> nuc
  );;
val transcription_nuc : char -> char = <fun>

# let rec transcription_aux sequence acc = (
    let len = String.length sequence in 
    if len = 0 then (* stopping case*)
      acc (* return the total transcript*)
    else 
      let transcript = acc ^ Char.escaped (transcription_nuc sequence.[0]) in
      let tail = String.sub sequence 1 (len-1) in 
      transcription_aux tail transcript (* recursive call*)
  );;
val transcription_aux : string -> string -> string = <fun>

# let rec transcription sequence = (
    transcription_aux sequence ""
  );;
val transcription : string -> string = <fun>
```

<div style="text-align: justify">
The <code>match</code> allows to compare an element, here the character, to a pattern, and to execute the associated function (after the <code>-></code>). The patterns of the match must be exhaustive, that's why we use the <code>_</code> which will match all the remaining patterns.

In the <code>transcription_nuc</code> function, we look if the character is 'T'. If it is, we return the character 'U', otherwise we return the base character, either 'A', 'C' or 'G' for the DNA. In the end, we modify only the 'T', as during the transcription.

For the function transcribing the whole sequence <code>transcription</code>, the stop case occurs when the sequence is empty. Otherwise, we transcribe the first nucleotide of the sequence and we concatenate it at the end of what we have already built (<code>acc</code>). The recursive call is made on the rest of the sequence. The last call of the function is the [recursive call](recursivefunc.md) so that we can do terminal recursion, improving the performance of our code.
</div>

<div style="text-align: center"> 

![schema recursive function for transcription](images/trans_rec.svg)
</div>

## Translation <a id="tra"></a>

<div style="text-align: justify">
Translation is a step where the ribosome binds to a coding sequence and converts each codon (sequence of 3 nucleotides) into an amino acid. Computationally, this is equivalent to taking a string of length multiple of 3, and returning a new string with the protein.
</div>
</br>
<div style="text-align: center"> 

![schema traduction](images/traduction.svg)
</div>
<br/>

### Get a codon table <a id="codon"></a>

<div style="text-align: justify">
To work on the translation without using too complicated functions, it would be interesting to have, like the dictionaries in Python. In OCaml, there are two alternatives, hash tables and maps (pair association). For this function, a .json file has been created (<code>codon_table.json</code>), with a JSON dictionary containing the codons as key, and the amino acids coming from the translated codon as value.

Here is an overview of the file :
</div>

```JSON
{
  "UUU":"F",
  "UUC":"F",
  "UUA":"L",
  "UUG":"L",
  "UCU":"S",
  "UCC":"S",
  ...
}
```

In Python, we could have done :

```python
dna = "ATGGGAUGUUGA"

with open("codon_table.json") as table:
    content = table.read()
    content = content.replace('\n','')
    content = content.replace('{','')
    content = content.replace('}','')
    content = content.replace(' ','')
    content = content.replace('"','')
    content = content.split(",")
    codontable = dict()
    for kv in content:
        k,v = kv.split(':')
        codontable[k]=v
```

<div style="text-align: justify">

To do the same thing in OCaml, we will have to deal with the string comprehension. To avoid having to code basic functions like "replace", we will use an OCaml module, "Str". This module has several functions to work on strings. It also has the advantage of allowing to work with regular expressions. To load the module, just do <code># load "str.cma"</code> at the beginning of the file.
</div>

```ocaml
# #load "str.cma";;
```

#### Make the file as a list <a id="file-list"></a>

<div style="text-align: justify">

First of all we will have to [read the file](read_file.md). To avoid errors, I use a function similar to with.
</div>

```ocaml
# let with_file path func = (
    let channel = open_in path in
    try
      let func_result = func channel in
      let () = close_in channel in 
      func_result
    with e -> let () = close_in channel in raise e
  );;
val with_file : string -> (in_channel -> 'a) -> 'a = <fun>

# let rec string_of_file ?(interline="\n") ?(already_read="") channel = (
    try
      let line = input_line channel in 
      string_of_file ~interline:"" ~already_read:(already_read ^ interline ^line) channel
    with End_of_file -> already_read
  );;
val string_of_file :
  ?interline:string -> ?already_read:string -> in_channel -> string = <fun>
```

<div style="text-align: justify">

PS : Don't hesitate to read the [read the file](read_file.md) sheet to understand how to read a file in OCaml!

Then we do the function to convert this long string into a list :
</div>

```ocaml
# let json2list path = (
    let jsonline = with_file path (string_of_file ~interline:"") in (*1*) 
    let jsonline_without_bracket = Str.global_replace (Str.regexp "[ \"{}]") "" jsonline in  (*2*)
    Str.split (Str.regexp ",") jsonline_without_bracket (*3*)
  );;
val json2list : string -> string list = <fun>

# let j_list = json2list "codon_table.json";;
val j_list : string list =
  ["UUU:F"; "UUC:F"; "UUA:L"; "UUG:L"; "UCU:S"; "UCC:S"; "UCA:S"; "UCG:S";
   "UAU:Y"; "UAC:Y"; "UAA:*"; "UAG:*"; "UGU:C"; "UGC:C"; "UGA:*"; "UGG:W";
   "CUU:L"; "CUC:L"; "CUA:L"; "CUG:L"; "CCU:P"; "CCC:P"; "CCA:P"; "CCG:P";
   "CAU:H"; "CAC:H"; "CAA:Q"; "CAG:Q"; "CGU:R"; "CGC:R"; "CGA:R"; "CGG:R";
   "AUU:I"; "AUC:I"; "AUA:I"; "AUG:M"; "ACU:T"; "ACC:T"; "ACA:T"; "ACG:T";
   "AAU:N"; "AAC:N"; "AAA:K"; "AAG:K"; "AGU:S"; "AGC:S"; "AGA:R"; "AGG:R";
   "GUU:V"; "GUC:V"; "GUA:V"; "GUG:V"; "GCU:A"; "GCC:A"; "GCA:A"; "GCG:A";
   "GAU:D"; "GAC:D"; "GAA:E"; "GAG:E"; "GGU:G"; "GGC:G"; "GGA:G"; "GGG:G"]
```

<div style="text-align: justify">

- 1 : The file is read and converted into a long string.
- 2 : All the characters we are not interested in are deleted. These characters are presented in the regex <code>"["{}"]"</code>. A regular expression is a string of characters with special characters symbolizing variations of this string. For example, the regex <code>"a"</code> represents the string <code>"a"</code> when <code>"[ab]"</code> represents the string <code>"a"</code> but also <code>"b"</code>. The square brackets show the possible choice between several characters, here the space (<code>" "</code>), the square brackets (<code>"{"</code> and <code>"}"</code>), and the quotation mark (<code>' " '</code>), which is escaped with <code>"\"</code> to avoid that OCaml thinks we close the string.
- 3 : As in Python, the string is separated into a list starting from the <code>":"</code> character. So we have a list of type <code>["AAA:X",...]</code>.

Let's see our list !
</div>

```ocaml
# let rec print_list = function
  | [] -> ()
  | e::l -> print_string e ; print_string "\n" ; print_list l;;
val print_list : string list -> unit = <fun>
```
<!-- $MDX skip -->
```ocaml
# print_list j_list;;
UUU:F
UUC:F
UUA:L
UUG:L
UCU:S
UCC:S
UCA:S
UCG:S
UAU:Y
UAC:Y
UAA:*
UAG:*
...
- : unit = ()
```

<div style="text-align: justify">

In addition, a small function has been made. It will just allow us to display the content of our tables.
</div>

```ocaml
# let print_iter k v = (
    print_string k;
    print_string " ";
    print_char v;
    print_endline "";
  );;
val print_iter : string -> char -> unit = <fun>
```

#### Convert the list as a hash table <a id="list-ht"></a>

<!-- diagrams a ajouter -->

<div style="text-align: justify">

<a href="hashtable.md">Hashtable</a> are the imperative way to view key-value associations in OCaml. Modifications are done in-place. To succeed in making one, several functions have been realized.
</div>

```ocaml
# let rec add_kv_hashcodon ht l = (
    match l with
    | [] -> (); (*1*)
    | hd::tl -> let kv = Str.split (Str.regexp ":") hd in (*2*)
    Hashtbl.add ht (List.nth kv 0) ((List.nth kv 1).[0]) ; (*3*)
    add_kv_hashcodon ht tl (*4*)
  );;
val add_kv_hashcodon : (string, char) Hashtbl.t -> string list -> unit =
  <fun>

# let hashcodon_of_list l = ( (* string, char hash table*)
    let codon_hash = Hashtbl.create 64 in (* create the hash table *)
    let () = add_kv_hashcodon codon_hash l in (* fill the hash table*)
    codon_hash
  );;
val hashcodon_of_list : string list -> (string, char) Hashtbl.t = <fun>
```

<div style="text-align: justify">

Two functions have been written: 
- <code>add_kv_hashcodon</code>: fills the hash table from the list created above
- <code>hashcodon_of_list</code> : create the hash table then fill it by calling the first function

The creation of the hash table takes as argument 64. This is the estimated size of the hash table (here 64 there are 64 possible codons). Predicting in advance the size of the hash table allows to improve the performances. However, this size is just an indication, we can very well put more or less elements in a hash size than the size given at the origin.

More details about <code>add_kv_hashcodon</code>:

1. Stop case: when the list is empty, I return <code>()</code>, of type 'unit'. In OCaml, this means 'nothing', an equivalent of Python's None,
2. Otherwise: I separate the head (the first element of the list) into a list of two elements, the key and the value that I will have to put in the hash table. As a reminder, the elements of the list are in the form <code>"AAA:X"</code>, so if I split on the character <code>":"</code>, I get the desired list,
3. Adding the key/value pair to the hash table,
4. The recursive call with the tail of the list.

Let's take a look at our table.

</div>

<!-- $MDX skip -->
```ocaml
# let codon_hash = hashcodon_of_list (json2list "codon_table.json");;
val codon_hash : (string, char) Hashtbl.t = <abstr>
# Hashtbl.iter print_iter codon_hash;;
AGC S
UGC C
AAC N
UAG *
CGU R
AUG M
AUC I
CAG Q
CUG L
UUC F
AAG K
...
- : unit = ()
```

#### Convert the list in a Map <a id="list-map"></a>

<!-- diagrams a ajouter -->

<div style="text-align: justify">

The map structure associates a value of type <code>'a</code> with a value of type <code>'b</code>. To define such a structure, in OCaml, we use the <code>module</code> keyword. Generally speaking, the <code>module</code> keyword is used to define a module (equivalent to a class in Python). We can either define values and methods (according to a syntax that will not be explained here), or use a Functor that will do it for us. There is the Function <code>Map.Make</code> which takes as argument a type and create a map structure which will take as key this argument. 
</div>

```ocaml
module CodonDict = Map.Make(String);;
```

<div style="text-align: justify">

Maps are recursive structures, each operation will return a new <code>Map</code> object. A function returning our Map filled with codon must be built.
</div>

```ocaml
# let rec mapcodon_of_list l = (
    match l with
    | [] -> CodonDict.empty (*1*)
    | hd::tl -> let kv = Str.split (Str.regexp ":") hd in (*2*)
        CodonDict.add (List.nth kv 0) ((List.nth kv 1).[0]) (mapcodon_of_list tl) (*3*)
  );;
val mapcodon_of_list : string list -> char CodonDict.t = <fun>
```
<div style="text-align: justify">

1. Stop case: If the list is empty, I return an empty Map,
2. Otherwise: I separate the head (the first element of the list) into a list of two elements, the key and the value that I will have to put in a new map table. As a reminder, the elements of the list are in the form <code>"AAA:X"</code>, so if I split on the character <code>":"</code>, I get the desired list,
3. Returns a new Map with the new key/value pair

Let's take a look at our table.

</div>

<!-- $MDX skip -->
```ocaml
# let codon_map = mapcodon_of_list (json2list "codon_table.json");;
val codon_map : char CodonDict.t = <abstr>
# CodonDict.iter print_iter codon_map;;
AAA K
AAC N
AAG K
AAU N
ACA T
ACC T
ACG T
ACU T
AGA R
AGC S
AGG R
...
- : unit = ()
```

### Doing the translation <a id="tra2"></a>

<div style="text-align: justify">

For translation, the logic is the same as for transcription. However, depending on the data structure used, the conversion of the codon into amino acid will not be the same.
</div>

<div style="text-align: center">

![trad](images/trad_rec.svg)

</div>

#### With hash table <a id="ht"></a>

```ocaml
# let rec translation_hash_aux ?(acc="") ?(pos=0) ht sequence = (
    if pos == String.length sequence then
      acc
    else 
      let amino_acid = Hashtbl.find ht (String.sub sequence pos 3) in 
      translation_hash_aux ~acc:(acc^(Char.escaped amino_acid)) ~pos:(pos+3) ht sequence
  );;
val translation_hash_aux :
  ?acc:string -> ?pos:int -> (string, char) Hashtbl.t -> string -> string =
  <fun>

# let translation_hash ht sequence = (
    if String.length sequence mod 3 != 0 then 
      failwith "No Coding Sequence (the length need to be a multiple of 3)"
    else translation_hash_aux ht sequence
  );;
val translation_hash : (string, char) Hashtbl.t -> string -> string = <fun>

# let codon_hash = hashcodon_of_list j_list;;
val codon_hash : (string, char) Hashtbl.t = <abstr>
# print_endline (translation_hash codon_hash "AUGGGAUAA");;
MG*
- : unit = ()
```

<div style="text-align: justify">

<code>translation_hash_aux</code> takes 4 arguments :
- <code>acc</code> : the already translated sequence in the previous recursive call
- <code>pos</code> : the position where the program extract the codon. It's possible to avoid using pos and, at each recursive call, give a truncated sequence, but if we do that, each new subsequence will take up space in the memory
- <code>ht</code> : the hash table
- <code>sequence</code> : the sequence we want to translate

The stopping case is when <code>pos</code> reaches the end of the sequence. Otherwise, we translate the first codon (3 first nucleotides), add 3 to the position and do our recursive call.

The function <code>translation_hash</code> is for the user who didn't need to have access to <code>acc</code> or <code>pos</code>. With this, we take the opportunity to check the conformity of the sequence. If its size is not a multiple of 3, we cannot translate it and we return an error. 

If we want to be sure that our sequence only contains nucleotid, we can even create a type [dna](dna.md).

</div>

#### With Map <a id="map"></a>

With Map, the code is almost the same that above, so the explication will be really similar.

```ocaml
# let rec translation_map_aux ?(acc="") ?(pos=0) table sequence = (
    if pos == String.length sequence then
      acc
    else 
      let amino_acid = CodonDict.find (String.sub sequence pos 3) table in 
      translation_map_aux ~acc:(acc^(Char.escaped amino_acid)) ~pos:(pos+3) table sequence
  );;
val translation_map_aux :
  ?acc:string -> ?pos:int -> char CodonDict.t -> string -> string = <fun>


# let translation_map table sequence = (
    if String.length sequence mod 3 != 0 then 
      failwith "No Coding Sequence (the length need to be a multiple of 3)"
    else translation_map_aux table sequence
  );;
val translation_map : char CodonDict.t -> string -> string = <fun>

# let codon_map = mapcodon_of_list j_list;;
val codon_map : char CodonDict.t = <abstr>
# print_endline (translation_map codon_map "AUGGGAUAA");;
MG*
- : unit = ()
```
<div style="text-align: justify">

<code>translation_map_aux</code> takes 4 arguments :
- <code>acc</code> : the already translated sequence in the previous recursive call
- <code>pos</code> : the position where the program extract the codon. It's possible to avoid using pos and, at each recursive call, give a truncated sequence, but if we do that, each new subsequence will take up space in the memory
- <code>table</code> : the hash table
- <code>sequence</code> : the sequence we want to translate

The stopping case is when <code>pos</code> reaches the end of the sequence. Otherwise, we translate the first codon (3 first nucleotides), add 3 to the position and do our recursive call.

The function <code>translation_ma</code> is for the user who didn't need to have access to <code>acc</code> or <code>pos</code>. With this, we take the opportunity to check the conformity of the sequence. If its size is not a multiple of 3, we cannot translate it and we return an error. 

If we want to be sure that our sequence only contains nucleotid, we can even create a type [dna](dna.md).

#### Main differences between Hashtbl and Map module for our code <a id="diff"></a>

- Hashtbl let us create an instance of hash table without the creation of a new module like Map,
- Fill the map is easier is OCaml because of it's recursive structure,
- Warning : The find function doesn't take argument in the same order ! For Hashtbl, the table need to be the first argument, but for Map, it has to be the last one.

</div>

## Conclusion <a id="conclusion"></a>

In the sheet, we'll see how to transribe and translate a sequence of nucleotide. We exclusively work on String, but the logic stay the same if you want to work with a [custom dna type](dna.md) to avoid unwanted nucleotid as a list of pre-set character. 

</div>