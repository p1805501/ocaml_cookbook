---
title: Recursive operations on List
---

<div id="menu_md"> 

## Table of Contents

<div class="toc_md"> 

- [Introduction](#intro)
- [Sum of the elements of a list](#sum)
- [The Fold_left function](#fold)

</div>
</div>

<div id="tuto"> 

# Recursive operations on List

## Introduction <a id="intro"></a>

<div style="text-align: justify"> 
This tutorial aims at learning how to make simple recursive operations on lists like :

- mathematical operations (<code>+</code>,<code>-</code>,<code>/</code>,<code>*</code>) between the elements of the list
- concatenation of the elements of the list

By the end of this tutorial we will be able to call a function giving a list as an argument like the following example.<br/>
</div>

<!-- $MDX skip -->
```ocaml
(* example of code to compute the sum of all the elements of a list*)
# let count_list = [1;2;3;4;5;6;7;8;9;10];;
# let sum_of_count = sum_list count_list ;;
- : int = 55
```

<div style="text-align: justify"> 

For the following steps we will take as an example the case where we want to compute the sum of all the elements of a list.<br/>
</div>

## Sum of the elements of a list <a id="sum"></a>

<div style="text-align: justify"> 

In imperative programming, to sum the elements of a list we would loop on the list with a counter to which we add the value of the current cell we are in. An example in python could be : <br/>
</div>

```python
# Definition of the list
count_list = [1,2,3,4,5,6,7,8,9,10]
def list_sum (l) :
    count=0
    for i in l :
        count += i
    return count
```

<div style="text-align: justify"> 

In order to solve this problem in functionnal programming we have to create a recursive function (to see more on [recursive functions](recursivefunc.html))

The stopping conditions here would be :

- the list is empty => return 0
- the list is not empty => we separate the first element (```head```) from the rest and we sum the first element to the sum of the rest of the list (```tail```)

The recursive function in OCaml would then look like this
</div>

```ocaml
let rec list_sum list =
  match list with
  | [] -> 0
  | h::t -> h + (list_sum t);;
```
![example of recursive sum on a list](images/Recursivefunc_lists.svg)

## The Fold_left function <a id="fold"></a>

<div style="text-align: justify"> 

A function exist in the module [List](https://v2.ocaml.org/api/List.html) that allow us to easily do recursion on a list with a variety of differents operations.

The function fold_left works like the following : 
</div>

<!-- $MDX skip -->
```ocaml
List.fold_left f init l ;;
```

<div style="text-align: justify"> 

With :

- f : the function we want to apply to the list
- init : the initial condition
- l : the list we want to make a recursion on

This function can be applied on int list as well as string or float lists.
</div>

```ocaml
# let count_list = [1;2;3;4;5;6;7;8;9;10];;
val count_list : int list = [1; 2; 3; 4; 5; 6; 7; 8; 9; 10]
# List.fold_left (+) 0 count_list;;
- : int = 55
```

```ocaml
# let dna_list = ["ACGTCCGA";"ATCGGGATA";"TAGCA";"TACCA"];;
val dna_list : string list = ["ACGTCCGA"; "ATCGGGATA"; "TAGCA"; "TACCA"]
# List.fold_left (^) "" dna_list;;
- : string = "ACGTCCGAATCGGGATATAGpandoc --standalone --template template.html ../book/gc_content.html -o gc_content.html CATACCA"
```

```ocaml
# let value_list = [15.7;20.8;4.19;10.99];;
val value_list : float list = [15.7; 20.8; 4.19; 10.99]
# (List.fold_left (+.) 0. value_list) /. (float_of_int (List.length value_list));;
- : float = 12.92
```

<div style="text-align: justify"> 

Now that we know how to do recursive operations on lists, you can continue your learning of the language with [learning to create a type](type.html).
</div>

</div>