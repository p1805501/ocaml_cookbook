---
title: Find a motif in a sequence
---

<div id="menu_md"> 

## Table of Contents

<div class="toc_md">

- [The biological problem](#bio)
- [Imperative solution in Python](#python)
- [Imperative solution in OCaml](#imperative)
- [Recursive solution](#recursive)
- [A better way to develop in OCaml](#better)
- [Print the solution](#print)
</div>
</div>

<div id="tuto"> 

# Find a motif in a sequence

## The biological problem<a id="bio"></a>

<div style="text-align: justify"> 
It is common in bioinformatics to want to find the number of occurrences and the position of a subsequence in a sequence.
<br/>
<br/>
</div>

<div style="text-align: center">

<img src="images/find_motif.svg"
     alt="schema findmotif">
</div>

<div style="text-align: center; font-size: 10px ">Diagram of a motif repeated 3 times</div>


<div style="text-align: justify"> 
Here, the <code>CGCT</code> pattern is represented 3 times in the total sequence, so we are looking for a program that gives us the 3 positions where we find this pattern.
To store the results, an integer list can be used. 
</div>
<br>

## Imperative solution in Python<a id="python"></a>

Let's start by seeing how we can solve this problem with an imperative approach, with Python.

```python
def find_motif(motif, sequence):
    res = []  # init the list of resultat
    length_motif = len(motif)
    for i in range(0, len(sequence)- length_motif +1):  # we go throught each position of the sequence
        if sequence[i:i+length_motif] == motif:  # if we find the motif in my sequence
            res.append(i)  # we add the solution to res
    return res
```
<div style="text-align: justify"> 

First, we want to go through each position of the sequence, in our example sequence, over 22 positions, to see if the motif starts there. So we have 22 opportunities where the motif can start. Except that if the motif starts but the rest of the sequence is too short for it to finish, we know that it will not be there. For our example, our pattern has a length of 4, so it is useless to go after position 18 because from position 18, there are only 3 letters left (19, 20, 21) so not enough for our motif . 

<code>i</code> will iterate over the sequence :
- from <code>0</code>,
- to <code>length(sequence) - length(motif) + 1 = 18</code>. 

Sequence indexing starts at <code>0</code>, so if <code>ls</code> is the length of the sequence, the index of the last element is <code>ls-1</code>. At index <code>ls-1</code>, we have only one character, so there is room for a motif of <code>1</code> character. At <code>ls-2</code>, there is room for a motif of <code>2</code> characters, etc. Finally, if <code>lm</code> is the length of the motif, we can search for the last motif at position <code>ls-lm</code>, so from <code>ls-lm+1</code>, we cannot search anymore.

</div>

<div style="text-align: center"> 

<img src="images/througth.svg"
     alt="i go from 0 to 18">

</div>

<div style="text-align: justify"> 

At each position, we look if the motif is there and :

- if the pattern is here : we add the position to our position's list
- else : we continue

</div>

## Imperative solution in OCaml <a id="imperative"> </a>

We can do imperative programming in OCaml with a code very close to the one done in Python.

This is what it will look like : 

```ocaml
# let find_motif motif sequence = 
    let res = ref [] in 
    let length_motif = String.length motif in 
    for i = 0 to (String.length sequence - length_motif) do 
      if (String.sub sequence i length_motif) = motif 
        then res := i :: !res; done; 
    !res;;
val find_motif : string -> string -> int list = <fun>
```

## Recursive solution<a id="recursive"></a>

<div style="text-align: justify"> 
However, in OCaml, we prefer to do <a href="recursivefunc.md">recursive work</a>.

To see the problem in a recursive way is to say that the positions of the motif in the sequence are :

- <code>0</code> if the motif is present from the first character, else nothing

AND

- the list of the positions of the motif in the remaining sequence (adding 1 to the checking position in our recursive call).

The recursive call looks like that : 
</div>

```python
find_motif(motif, "NNNNNNN", position=0) =
 if is_motif(position=0):
  0 :: find_motif(motif, "NNNNNN")
else:
  find_motif(motif, "NNNNNN", position=position+1)
```
With each recursive call, the size of the sequence is reduced by 1.

A first approach is as follows :

```ocaml
# let rec find_motif ?(pos=0) motif sequence = (
    let lm = String.length motif in 
    let ls = String.length sequence in 
    if lm > ls then [] (* 1 *)
    else match ls with
    | x when x=lm -> 
        if motif = sequence then 
          pos :: [] 
        else [] (* 2 *)
    | _ -> 
      if motif = (String.sub sequence 0 lm) then
       pos :: find_motif motif (String.sub sequence 1 (ls-1)) ~pos:(pos+1)
      else find_motif motif (String.sub sequence 1 (ls-1)) ~pos:(pos+1) (* 3 *)
  );;
val find_motif : ?pos:int -> string -> string -> int list = <fun>
```
- (1) Stopping Case : when the motif is smaller than the sequence itself. This takes into account the case where the sequence is empty or the case where the input sequence is too small. If there is no motif, then an empty list is returned.
- (2) Trivial Case : if the motif and the sequence have the same length, we just have to check if the sequence is the motif and return a list with position 0 if it is the case, an empty list otherwise
- (3) If the previous cases are not verified, we make the recursive call with the reduced sequence and by increasing pos

Note : here <code>pos</code> is an optional argument of the function with default value 0.

```
# We define an optional argument with (it can't be the last argument) :
?(argument_name=default_value)
# We call it with :
function ~argument_name:value args1 args2
```

<div style="text-align: justify"> 
It is used in our case during the recursive call to remember how many positions we have already processed, since we are looking for the motif at the beginning of the sub-sequence. This means that if we find our motif at position 0 of our subsequence, it means that it is actually at position <code>pos</code> of our main sequence because we have already looked at <code>pos</code> position before.
</div>
<br/>

## A better way to develop in OCaml<a id="better"></a>
<div style="text-align: justify"> 

The previous code, although functional, can be improved. It can't work and overload the memory on too big data set. Each track of improvement will be seen in this part.
</div>

### Useless condition
<div style="text-align: justify"> 

The trivial case can be removed. This is equivalent to making the recursive call on it and when the function is called again with the shortened sequence, its length goes below that of the motif and triggers the stop case.
</div>

```ocaml
# let rec find_motif ?(pos=0) motif sequence = (
    let lm = String.length motif in 
    let ls = String.length sequence in 
    if lm > ls 
      then []
      else 
        if motif = (String.sub sequence 0 lm) 
          then pos :: find_motif motif (String.sub sequence 1 (ls-1)) ~pos:(pos+1) 
          else find_motif motif (String.sub sequence 1 (ls-1)) ~pos:(pos+1)
  );;
val find_motif : ?pos:int -> string -> string -> int list = <fun>
```

### Terminal recursion
<div style="text-align: justify"> 

[Terminal recursion](recursivefunc.md) is a principle applicable to recursive functions. If the last statement is the recursive call, then the compiler treats the function as a <code>for</code> loop and the program gains memory performance. This induces changes in the program parameters.
</div>

```ocaml
# let rec find_motif ?(pos=0) ?(acc=[]) motif sequence = (
    let lm = String.length motif in 
    let ls = String.length sequence in 
    if lm > ls 
      then acc 
      else 
        let pos_list = if motif = (String.sub sequence 0 lm) 
          then pos :: acc
          else acc
        in
      find_motif ~pos:(pos+1) ~acc:pos_list motif (String.sub sequence 1 (ls-1))
  );;
val find_motif : ?pos:int -> ?acc:int list -> string -> string -> int list =
  <fun>
```

### Don't use String.sub when it's not necessary

<div style="text-align: justify"> 
During the recursive call, we use the <code>String.sub</code> function to obtain the sequence without its first character.<br/> The problem with this way of doing things is that at each recursive call another string is created and stored in memory.<br/> However, we use <code>pos</code> in our function, and we could use it to just test the motif from <code>pos</code> instead of from the beginning of the sequence. This also changes the stopping case: we don't compare the length of the motif to the length of the sequence but to the length of the sequence - pos.
</div>

```ocaml
# let rec find_motif ?(pos=0) ?(acc=[]) motif sequence = (
    let lm = String.length motif in 
    let ls = String.length sequence - pos in 
    if lm > ls 
      then acc 
      else 
        let pos_list = if motif = (String.sub sequence pos lm) 
          then pos :: acc
          else acc
        in
      find_motif ~pos:(pos+1) ~acc:pos_list motif sequence 
  );;
val find_motif : ?pos:int -> ?acc:int list -> string -> string -> int list =
  <fun>
```

### Check if the motif is present differently

<div style="text-align: justify"> 

The condition <code>if motif = (String.sub sequence pos lm)</code> can be improved for performance reasons. As we saw in the previous paragraph, using <code>String.sub</code> will create a new sequence (here of size <code>lm</code>), and keep it in memory. 

We can do better by comparing just the first <code>lm</code> characters of the sequence and the motif (i.e. the whole motif). In Python, this corresponds to the <code>startsWith</code> function.
</div>

```ocaml
# let rec isAt_aux ?(it=0) ?(pos=0) motif sequence = (
    if it = String.length motif
      then true
      else sequence.[pos+it] = motif.[it] && isAt_aux ~it:(it+1) ~pos:pos motif sequence  
  );;
val isAt_aux : ?it:int -> ?pos:int -> string -> string -> bool = <fun>

# let rec isAt ?(pos=0) motif sequence = (
    if String.length motif > String.length sequence - pos
      then false
      else isAt_aux ~pos:pos motif sequence
  );;
val isAt : ?pos:int -> string -> string -> bool = <fun>
```
<div style="text-align: justify"> 

The <code>isAt_aux</code> function is the function that replaces the <code>startswith</code> of Python. It's a boolean function which return True if a motif is present at a given position. It takes two optional parameters, <code>pos</code> to ask for the position where we want to look for the motif and <code>it</code> which is used for recursion. Normally, the <code>it</code> parameter should not be modifiable by the user, hence the auxiliary function. The main function is the one that should be called without any optional "operating" parameters.
</div>

## Use an auxiliary function so that the user can use all optional parameters

<div style="text-align: justify"> 

The remark in the previous paragraph for the <code>isAt</code> function can also be applied to find_motif.
One can debate the usefulness of the parameter pos, which can be used to indicate the position from which to start the search. However, acc is only used for recursion, so it is necessary to make a main function with no parameter.
</div>

```ocaml
# let rec find_motif_aux ?(pos=0) ?(acc=[]) motif sequence = (
    let lm = String.length motif in 
    let ls = String.length sequence - pos in 
    if lm > ls then
     acc 
    else let pos_list = 
      if isAt ~pos:pos motif sequence then 
        pos :: acc
      else acc
      in
      find_motif ~pos:(pos+1) ~acc:pos_list motif sequence 
  );;
val find_motif_aux :
  ?pos:int -> ?acc:int list -> string -> string -> int list = <fun>

# let find_motif ?(pos=0) motif sequence = (
    find_motif ~pos:pos motif sequence
  );;
val find_motif : ?pos:int -> string -> string -> int list = <fun>
```

## Print the solution<a id="print"></a>

Here's a way to print an integer list.

```ocaml
# let rec print_list = function
  | [] -> ()
  | e::l -> print_int e ; print_string " " ; print_list l;;
val print_list : int list -> unit = <fun>
```

We can now see our result :

```ocaml
# print_list (find_motif "CGCT" "ACGCTCCGCTCGTACGATCGCT");;
18 6 1
- : unit = ()
```

</div>
