---
title: Write in a fasta file
---

<div id="menu_md"> 

## Table of Contents

<div class="toc_md"> 

- [Introduction](#intro)
- [Writing a fasta sequence](#list)
  - [Open a file](#open)
  - [An example to write in a file](#write)
- [Writing multiple sequences](#fastas)
</div>
</div>

<div id="tuto"> 

# Write in a fasta file

## Introduction<a id="intro"></a>
<div style="text-align: justify"> 
This tutorial aims at learning how to functionaly write a sequence or a group of sequences in a fasta file, using OCaml. In bioinformatics, it is a common way to study sequences.
The fasta format offers to represent nucleotidic or proteic sequences, and informations, such as the name of the sequence, can be added before the sequence itself, through a line starting with a '>'.<br>
An example of fasta representation :<br>
</div>

<div class="sourceCode">
<code class="other">
>id1 and comment
<span style="overflow-wrap: break-word;">
MADQLTEEQIAEFKEAFSLFDKDGDGTITTKELGTVMRSLGQNPTEAELQDMINEVDADGNGTIDFPEFLTMMARKMKDTDSEEEIREAFRVFDKDGNGYISAAELRHVMTNLGEKLTDEEVDEMIREADIDGDGQVNYEEFVQMMTAK*
</span>
</code>
</div>

<div style="text-align: justify"> 
By the end of this tutorial we will be able to write sequences in a fasta file from a list of pairs of two strings (the id of the sequence and the sequence). <br>
</div>

<!-- $MDX skip -->
```ocaml
write_multi_fasta file_name1 seq_list;;
```

<div style="text-align: justify"> 
In imperative programming, we would be looping on the list and write each tuple in two lines for the id and the sequence. <br>
</div>

```python
list_seq = [("id1 and comment", "MADQLTEEQIAEFKEAFSLFDKDGDGTITTKELGTVMRS \
LGQNPTEAELQDMINEVDADGNGTIDFPEFLTMMARKMKDTDSEEEIREAFRVFDKDGNGYISAAELRHVMT \
NLGEKLTDEEVDEMIREADIDGDGQVNYEEFVQMMTAK*"), ("id2 and comment", "MADQLTEE \
QIAEFKEAFSLFDKDGDGTITTKELGTVMRSLGQNPTEAELQDMINEVDADGNGTIDFPEFLTMMARKMKDT \
DSEEEIREAFRVFDKDGNGYISAAELRHVMTNLGEKLTDEEVDEMIREADIDGDGQVNYEEFVQMMTAK*")]

my_file = "my_fasta_file.fasta"

def write_fasta(name_file):
  fasta_file = open(name_file, "w")

  for seq in list_seq:
    ofile.write(">" + seq[0] + "\n" + seq[1] + "\n")

  fasta_file.close()

write_fasta(my_file)

```

<div style="text-align: justify"> 
In functionnal programming, variables are immutable and instead of looping we recursively call functions, therefore we can't have the same approach. So in order to solve this problem, this tutorial offers to go through two steps:<br><br>

- Learning how to open and write in a file
- Adding multiple sequences in the same function

</div>

## Writing a fasta sequence in a file<a id="fasta"></a>

### Open a file<a id="open"></a>

<div style="text-align: justify"> 

In OCaml, depending on wether we want to read or write in a file, the method will be different (see [read file](./read_file.md) for more informations). Here, what we want is to write in an existing file or create the requested file with the requested sequences.
In order to do that, we can use the function <code>open_out_gen</code> taking as arguments a list specifying the opening mode (here to either append to an existing file or create the said file), the permission of the file that could be created and the name of the file to open.

</div>

<!-- $MDX skip -->
```ocaml
let open_f = open_out_gen [Open_append; Open_creat] 1411 name_file
```

<div style="text-align: justify"> 

Then, when we are done writing in the file, it is important to close it with the function <code>close_out</code>. 

</div>

<!-- $MDX skip -->
```ocaml
close_out open_f
```

### Write in a file<a id="write"></a>

<div style="text-align: justify">

To learn how to write in a file, let's see an example with a list of string containing the id/comment and its sequence. The argument given to the function are the name of the fasta file and the list of id and sequence.

Now that we know how to open a file to write in it, we can use the method <code>Printf.fprintf</code> which take as argument the destination of the string, the format of the string, and all arguments needed in the format, here the format of the string we want would be written ">%s\n%s\n", where <code>%s</code> means we want to insert a string type.

</div>

```ocaml
# let write_fasta file lines = (*write fasta : id + seq*)
    let open_f = open_out_gen [Open_append; Open_creat] 1411 file in
    Printf.fprintf open_f ">%s\n%s\n" (List.hd lines) (List.hd (List.tl lines));
    close_out open_f;;

write_fasta file_name seq;;

val write_fasta : string -> string list -> unit = <fun>
```

## Writing multiple sequences<a id="fastas"></a>

<div style="text-align: justify"> 

We are now able to open and write in a file, so let's see how to recursively write multiple fasta sequences.

When implementing a recursive function, an important thing to think about is the stopping condition. There may be several, and they are conditions that won't call the function recursively, but stop it. For more information, go to [Recursive function](./recursivefunc.md).

Here, the recursion will be applied on the list of pairs, so the stopping condition would be : the list is empty.

To do that in Ocaml, we can use the [pattern matching](./recursivefunc.md#implement), relatively easy to use as we are working on list.

* The list is empty = writes an empty string and close the file
* Any other case = writes a pair of id/sequence, close the file and recursively call the function on the tail of the list.
</div>

```ocaml
# let rec write_multi_fasta file lines = (*write multiple sequence in fasta format*)
    let open_f = open_out_gen [Open_creat; Open_append] 1411 file in
    match lines with
    |[] -> Printf.fprintf open_f "" ; close_out open_f
    |(h,n)::t -> Printf.fprintf open_f ">%s\n%s\n" h n ; close_out open_f ; write_multi_fasta file t ;;

write_multi_fasta file_name1 seq_list;;

val write_multi_fasta : string -> (string * string) list -> unit = <fun>
```

### Note

<div style="text-align: justify">

In the syntax <code>(h,n)::t</code>, <code>(h,n)</code> matches the first pair of the list with <code>h</code> the first element of the pair, ie the id of the sequence and <code>n</code> the second element of the pair, here, the sequence. <code>t</code> is the rest of the list.

</div>
</div>
