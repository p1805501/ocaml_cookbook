---
title: Compute the GC-content of a sequence
--- 

<div id="menu_md"> 

## Table of Contents

<div class="toc_md"> 

- [The biological problem](#intro)
- [Imperative solution in Python](#python)
- [Imperative solution in OCaml](#imperative)
- [Recursive solution](#recursive)
- [To go further](#type)
</div>
</div>

<div id="tuto"> 

# Compute the GC-content of a sequence

## The biological problem <a id="intro"></a>

<div style="text-align: justify"> 

In bioinformatics, the GC-content represent the percentage of Guanine (G) and Cytosine (C) in a DNA or RNA sequence.<br/>
This data can be used to identify certain characteristics of some sequences or even some specifics genes more or less GC-rich ...
</div>

![compute of gc content example](images/gc-content.svg)

<div style="text-align: justify">

In this example, there is 4 occurences of G and C.<br/>
As the length of the sequence is equal to 16, the GC-rate in this sequence is 0.25 (a percentage of 25%).

Thus, we want to build a function able to give us this value giving a sequence as an argument like the following example.<br/>
</div>

<!-- $MDX skip -->
```ocaml
# let sequence = "ACGAAATAAAACGTAA";;
# gc_content sequence;;
- : float = 0.25
```

## Imperative solution in Python<a id="python"></a>

<div style="text-align: justify">

Let's start by seeing how to solve this problem with an imperative approach in Python.
</div>

```python
#example in python
def gc_content (seq):
  count = 0
  if len(seq) = 0 :
    raise ValueError ("the string is empty")
  else :
    for i in seq :
      if i.upper() in ["C","G"] :
        count += 1
      elif i.upper() not in ["A","T"] :
        raise ValueError ("the sequence is not DNA")
  return (count/(len (seq)))
```

<div style="text-align: justify">

First of all, we have to check if our sequence is empty.<br/>
If not we can go through each position of the sequence and check if :

- the character is a G or a C -> we add one to the counter
- the character is neither A nor T -> we raise an error as the sequence is not DNA. 

Finally we can divide our counter by the length of the sequence.
</div>

## Imperative solution in OCaml <a id="imperative"> </a>

<div style="text-align: justify">

A strict conversion of this imperative function in the OCaml langage would then be :
</div>

```ocaml
# let gc_content seq =
  let count = ref 0. in 
  if String.length seq = 0 then failwith ("the sequence is empty")
  else 
    for i = 0 to String.length seq - 1 do
      count := 
        match Char.uppercase_ascii seq.[i] with
        | 'C' | 'G' -> !count +. 1.;
        | 'A' | 'T' -> !count +. 0.;
        | _ -> failwith ("This sequence is not DNA");
      done ;
    !count /. (float_of_int (String.length  seq));;
val gc_content : string -> float = <fun>
```

## Recursive solution <a id="recursive"></a>

<div style="text-align: justify">

However, in OCaml, we prefer to do recursive work.<br>
Thus, we would create 2 functions as the following example :
</div>

```ocaml
let rec gc_number seq =
  match (String.length seq) with
  | 0 -> 0. 
  | _ -> 
    let count = 
    match seq.[0] with 
    | 'C' | 'G' -> 1. 
    | 'A' | 'T' -> 0. 
    | _ -> failwith ("This sequence is not DNA") 
    in
    count +. (gc_number (String.sub seq 1 ((String.length seq)-1)) ) ;;  
```
<code>gc_number</code> :

<div style="text-align: justify">

It works as the following :
</div>

![gc_number execution example](images/gc-content_1.svg)

<div style="text-align: justify">

In this recursive function, we compute the number of C and G in the sequence :

- We go through the sequence and if we have :

  - a C or a G : count = 1
  - an A or a T : count = 0
  - anything else : an error occur as the sequence is not DNA
- Finally we add this value to the value obtained on the rest of the sequence via the recursive call of the function.
</div>

```ocaml
let gc_content seq = 
  if String.length seq = 0 then failwith ("The sequence is empty") (* 1 *)
  else (
    let number = gc_number seq in
    number /. (float_of_int (String.length seq)) (* 8 *)
  );;
```

<div style="text-align: justify">

The second function, <code>gc_content</code>, will simply call <code>gc_number</code> and divide the result returned by the function, by the length of the sequence
</div>

## Terminal recursion <a id="terminal"></a>

<div style="text-align: justify">

This function can be improved by doing terminal recursion on the recursive function (see more about terminal recursion [here](recursivefunc.html)).
</div>

![gc_number with terminal recursion](images/gc-content_2.svg)

<div style="text-align: justify">

Here is an example of what the recursive function ```gc_number```  with terminal recursion could look like :
</div>

```ocaml
# let rec gc_number ?(acc=0.) seq = 
  match (String.length seq) with
  | 0 -> acc 
  | _ -> 
    let count = 
    match seq.[0] with 
    | 'C' | 'G' -> 1. 
    | 'A' | 'T' -> 0. 
    | _ -> failwith ("This sequence is not DNA") 
    in
    gc_number ~acc:(acc +. count) (String.sub seq 1 ((String.length seq)-1)) ;;
val gc_number : ?acc:float -> string -> float = <fun>
```
<div style="text-align: justify">

Thus, our final function needs to be modified a bit when calling <code>gc_number</code> so that the user doesn't have to worry about the accumulator
</div>

```ocaml
# let gc_content seq = 
  if String.length seq = 0 then failwith ("The sequence is empty") (* 1 *)
  else (
    let number = gc_number ~acc:0. seq in
    number /. (float_of_int (String.length seq)) (* 8 *)
  );;
val gc_content : string -> float = <fun>
```
<div style="text-align: justify">

This solution can work. However it is not efficient as it forces us to verify the sequence (if it is a DNA sequence) each time we call that function.
</div>

## To go further using a type <a id="type"></a>

<div style="text-align: justify">

To solve this problem we can create a <code>dna</code> type (see more [here](type.html))

To remind you of what the <code>dna</code> type looks like, here is the basic functions of our type we defined [here](type.html).
</div>

```ocaml
# type nucleotide = A | C | G | T ;;
type nucleotide = A | C | G | T

# type dna = Sequence of nucleotide list;;
type dna = Sequence of nucleotide list

# let nuc_of_char character = 
  match (Char.uppercase_ascii character) with
  | 'A' -> A
  | 'C' -> C
  | 'G' -> G
  | 'T' -> T
  | _ -> failwith ("This sequence is not DNA");;
val nuc_of_char : char -> nucleotide = <fun>

# let dna_of_string seq = 
  let rec nuclist_of_string sequence =
    match (String.length sequence) with
    | 0 -> []
    | _ -> nuc_of_char (sequence.[0]) :: nuclist_of_string (String.sub sequence 1 ((String.length sequence)-1))
  in
  let nuc_list = nuclist_of_string seq in
  Sequence nuc_list;;
val dna_of_string : string -> dna = <fun>
```
<div style="text-align: justify">

So if we consider the <code>dna</code> type we created we can now define our function this way :
</div>

```ocaml
# let rec gc_number ?(acc=0.) (Sequence seq) = 
  match seq with
  | [] -> acc
  | h::t -> 
    let count =
    match h with
    | C | G -> 1. 
    | A | T -> 0.
    in
    gc_number ~acc:(acc +. count) (Sequence t);;
val gc_number : ?acc:float -> dna -> float = <fun>

# let gc_content (Sequence seq) = 
  if List.length seq = 0 then failwith ("The sequence is empty")
  else (  
    let number_gc = gc_number ~acc:0. (Sequence seq) in
    number_gc /. (float_of_int (List.length seq))
  );;
val gc_content : dna -> float = <fun>
```
<div style="text-align: justify">

- First we need to check if the sequence is empty or not, as the definition of our type allows sequences to be empty
- Then we define the recursive function to count the number of G and C in the sequence :
  - if the sequence is empty -> return 0.
  - if the sequence is not empty -> we check the first element of the list
    - if it is a C or a G -> count = 1.
    - if it is an A or a T -> count = 0.
  - Then, we add this count to the count returned by the function applied to the rest of the DNA sequence

Now that you know how to compute the GC-content of a sequence, you can continue your learning of the language with the [Write in a fasta file](writefasta.html) tutorial.

</div>
</div>