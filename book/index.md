---
title: OCaml Cookbook
--- 

<div id="tuto"> 

# An OCaml Cookbook for bioinformatics

## Introduction

<div style="text-align: justify"> 

This cookbook aims at teaching bioinformaticians basics operations in functional programing with the OCaml language.

Following this introduction, you will find multiple tutorials for commons operations in the bioinformatic field.
</div>

## Get started with OCaml

<div style="text-align: justify">

You can visit the official <a href="https://ocaml.org/">OCaml website</a> to help you <a href="https://ocaml.org/docs/up-and-running">install it</a>.

Then, we recommand installing <a href="https://github.com/ocaml/merlin"><code>Merlin</code></a>. It's an extension for code editor like VSCode or VSCodium. It let you visualize type of your expression when you write code in OCaml.
</div>

## Tutorials

Our tutorials are made for beginner, but you'll have a bad time if you start reading them without ever having seen OCaml. We strongly advise you to practice doing very basic scripts and to learn the rudiments before using this cookbook. 

<a href="https://ocaml-learn-code.com/learn/index_en.html">OCaml : Learn and Code</a> is a very valuable resource for getting started in OCaml. Only the superficial basics are covered, but that will be enough to tackle the cookbook later. 

### Understand elements of OCaml language

Here you'll find some tutorial to help you understand and apprehend elements of OCaml language.

<div style="text-align: justify">

<a href="recursivefunc.html">A first approach of recursive functions</a></br>
<a href="list_recursive_operation.html">Recursive operations on List</a></br>
<a href="hashtable.html">Hashtable in OCaml</a></br>
<a href="read_file.html">Read a file in Ocaml</a></br>

</div>

### More advanced use of OCaml for bioinformatician 

Don't worry and be happy with those documents to help you use OCaml for your bioinformatic duties.

<div style="text-align: justify">

<a href="type.html">Represent a DNA sequence with a type</a></br>
<a href="countkmer.html">Count k-mers in a sequence</a></br>
<a href="find_motif.html">Find a motif in a sequence</a></br>
<a href="gc_content.html">Compute the GC-content of a sequence</a></br>
<a href="writefasta.html">Write in a fasta file</a></br>
<a href="tra.html">Transcript and translate a sequence</a></br>
</div>

All documents in this cookbook except the images are licensed under <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">CC BY-NC-SA 4.0</a>.

<img src="images/by-nc-sa.svg"
     alt="licence cc">

</div>

