# Ocaml_cookbook

This repo contains a cookbook for functionnal programing in Ocaml. It aims at teaching bioinformaticians basics operations in functional programing with the OCaml language.

Our tutorials are made for beginner, but we strongly advise to practice doing very basic scripts and to learn the rudiments before using this cookbook.

[OCaml : Learn and Code](https://ocaml-learn-code.com/learn/index_en.html) is a very valuable resource for getting started in OCaml. Only the superficial basics are covered, but that will be enough to tackle the cookbook later.

Have fun :) 

## Information for futur editors of th cookbook

### Organisation of Markdown documents

In order to use the same css sheet, some `div` must be added in Markdown files:

* One `<div id = "menu_md">`, containing the table of content of the tutorials and that will be hide when convert to HTML file.
* One `<div id = "tuto">`, containing the rest of the page.


### Markdown to HTML conversion

We use `pandoc` to convert Markdown files into HTML files.

Example of usage when beeing in the `public` file:

`pandoc --standalone --template template.html ../book/gc_content.md -o gc_content.html `

Some changes to do after conversion:

* Change all `.md` links into `.html` links.
* Change class `<button>` of the page from `class="collapsible"` to `class="collapsible active"` and remove link.
* Add `id="view"`to the following `<div class="content">`.
