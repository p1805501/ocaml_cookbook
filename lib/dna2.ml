type nucleotide = A | C | G | T ;;

type dna = Sequence of nucleotide list;;

let char_to_nuc character = 
  match (Char.uppercase_ascii character) with
  | 'A' -> A
  | 'C' -> C
  | 'G' -> G
  | 'T' -> T
  | _ -> failwith ("This sequence is not DNA");;

let nuc_to_char nuc = 
  match nuc with
  | A -> "A"
  | C -> "C"
  | G -> "G"
  | T -> "T";;

(* let rec seq_to_dna sequence =
  match (String.length sequence) with
  | 0 -> []
  | _ -> char_to_nuc (sequence.[0]) :: seq_to_dna (String.sub sequence 1 ((String.length sequence)-1));; *)

let is_nuc = function
  | 'A'|'C'|'G'|'T' -> true
  | _ -> false;;

let rec is_dna seq = 
  if (String.length seq) = 0 then true
  else  
  match (String.length seq) with
  | 0 -> true
  | _ -> (is_nuc seq.[0]) && is_dna (String.sub seq 1 ((String.length seq)-1));; 

let adn = "ACGAAATAAAACGTAA";;
let arn = "AKDJHFPZKD";;
let dna_sequence = Sequence (seq_to_dna adn);;

let seq_dna seq = 
  let rec seq_to_dna sequence =
    match (String.length sequence) with
    | 0 -> []
    | _ -> char_to_nuc (sequence.[0]) :: seq_to_dna (String.sub sequence 1 ((String.length sequence)-1))
  in
  let nuc_list = seq_to_dna seq in
  Sequence nuc_list;;


seq_dna adn ;;

print_endline(string_of_bool (is_dna adn));;