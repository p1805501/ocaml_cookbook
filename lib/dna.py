def is_dna (seq):
    for i in seq :
        if i.upper() not in ["A","C","G","T"] :
            return False
    return True
sequence = "ACGAAATAaAACGTAA"
print(is_dna (sequence))