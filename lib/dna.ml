let adn = "ACGAAATAAAACGTAA";;
let arn = "AKDJHFPZKD";;

type dna = A | C | G | T | U;;

let nucleotide = function
| "A" -> A
| "C" -> C
| "G" -> G
| "T" -> T
| _ -> failwith ("This sequence is not DNA");;

let nuc = function
| A -> "A"
| C -> "C"
| G -> "G"
| T -> "T"
| U -> "";;

let is_dna char = 
  match char with 
|"A"|"C"|"G"|"T"|"a"|"c"|"g"|"t" -> true
|"" -> true
| _ -> false;;

let rec print_dna seq = 
  match (String.length seq) with
  | 0 -> ""
  | _ -> if is_dna (String.sub seq 0 1) = true
    then((String.sub seq 0 1) ^ print_dna (String.sub seq 1 ((String.length seq)-1)))
    else failwith ("This is not a DNA sequence");;

let sequence = print_dna adn;;
print_endline(sequence);;


