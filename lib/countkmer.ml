exception Invalid_argument;;

let rec list_kmer k seq acc= (* Recursive function listing all k-mers of a sequence *)
  let len = (String.length seq) in
  if k > len then (* Raises an Invalid_argument if the length of k-mer given is bigger than the sequence itself *)
    raise Invalid_argument
  else begin
    match len with (* Stopping conditions : the sequence is empty or the string is the size of the k-mer*)
    |0 -> List.rev acc
    |n when n = k -> List.rev (seq :: acc) 
    |_ -> list_kmer k (String.sub seq 1 (len-1)) ((String.sub seq 0 k) :: acc)
    (*|_ -> String.sub seq 0 k :: list_kmer k (String.sub seq 1 (len-1))*) (* Starts list with first k-mer + Recursive call on the sequence minus the first character *)
  end;;

let rec count_uniq uniq full acc = (* Recursive function *)(*acc*)
  match uniq with
  |[]->List.rev acc (*List.rev acc*)
  (* |h::t-> (h^"", List.length (List.filter (fun x -> x=h) full)) :: count_uniq t full;; *)
  |h::t -> count_uniq t full ((h^"", List.length (List.filter (fun x -> x=h) full)) :: acc);;

let rec print_pairs list = (* Recursive function printing pairs of k-mer/iteration *)
  match list with (* Stopping conditions : empty list *)
  |[]->""
  |(h,n)::t->h^" - "^(string_of_int n)^"\n"^(print_pairs t);; 


let list = list_kmer 3 "ACGAAATAAAACGTAA" [] in
print_pairs (count_uniq (List.sort_uniq String.compare list) list [])
|> print_string;;


(* let print_int_pair_list list = 
  let string_of_int_pair pair = 
    match pair with
    |a,b -> a^" - "^(string_of_int b)^"\n" in
    List.map string_of_int_pair list 
    |> List.iter print_string;; *)

(* let count_unique_elements list = (* Function listing pairs of unique k-mers and their iteration *)
  let count_element e list = (* Function counting repetition of e in list *)
    List.filter (fun x -> x = e) list (* Creates list with all es in list *)
  |> List.length in (* Gets length of list of es *)
  List.sort_uniq String.compare list 
  |> List.map (fun e -> (e, count_element e list));; *)

(* count_unique_elements (list_kmer 3 "ACGAAATAAAACGTAA")
|> print_pairs
|> print_string;; *)