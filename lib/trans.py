dna = "ACGCGCGAGGGTGA"

dna = dna.replace("T","U")

print(dna)


dna = "ACGCGCGAGGGTGA"

def transcription_nuc(nucleotide):
    if nucleotide == 'T':
        return 'U'
    else:
         return nucleotide

def transcription(sequence):
    transcript = ""
    for char in sequence:
        transcript += transcription_nuc(char)
    return transcript

print(transcription(dna))
