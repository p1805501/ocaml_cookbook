open Str;;

let rec transcription_nuc nuc = (
  if Char.uppercase_ascii nuc = 'T' then
    'U'
  else Char.uppercase_ascii nuc
);;

let transcription_nuc nuc = (
  match Char.uppercase_ascii nuc with
  | 'T' -> 'U'
  | _ -> nuc
);;

let rec transcription sequence = ( 
  let transcript = ref "" in
  for i = 0 to String.length sequence - 1 do
    transcript := !transcript ^ Char.escaped (transcription_nuc sequence.[i]);
  done;
  !transcript
);;

let rec transcription_aux sequence acc = (
  let len = String.length sequence in 
  if len = 0 then (* stopping case*)
    acc (* return the total transcript*)
  else 
    let transcript = acc ^ Char.escaped (transcription_nuc sequence.[0]) in
    let tail = String.sub sequence 1 (len-1) in 
    transcription_aux tail transcript (* recursive call*)
);;

let rec transcription sequence = (
  transcription_aux sequence ""
);;

let dna = "ACGCGCGAGGGTGA";;
print_endline (transcription dna);;

(* let adn_comp nuc = (
  match String.uppercase_ascii nuc with
  | "A" -> "T"
  | "C" -> "G"
  | "G" -> "C"
  | "T" -> "A"
  | _ -> raise (Invalid_argument "Not a Nucleotide")
);;

let transcription_nuc nuc = (
  match String.uppercase_ascii nuc with
  | "T" -> "U"
  | "A" | "C" | "G" -> nuc
  | _ -> raise (Invalid_argument "Not a Nucleotide")
);;

let rec transcription sequence = ( 
  let l = String.length sequence in
  match l with
  | 0 -> "" 
  | 1 -> transcription_nuc (Char.escaped sequence.[0])
  | _ -> transcription_nuc (Char.escaped sequence.[0]) ^ (transcription (String.sub sequence 1 (l-1)))
);;

print_endline (transcription "ATCTACT") *)
