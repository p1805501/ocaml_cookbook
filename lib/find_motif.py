def find_motif(motif, sequence):
    res = []  # init the list of resultat
    length_motif = len(motif)
    for i in range(0, len(sequence)- length_motif +1):  # i go throught each position of the sequence
        if sequence[i:i+length_motif] == motif:  # if i find the motif in my sequence
            res.append(i)  # i add the solution to res
    return res

x = find_motif("GCG","ACGCGCGA")
print(x)