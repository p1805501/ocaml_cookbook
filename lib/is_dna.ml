let is_seq_dna seq =
  let is_nuc character = 
  match (Char.uppercase_ascii character) with
    | 'A'|'C'|'G'|'T' -> true
    | _ -> false;
  in
  let rec is_dna seq = 
    if (String.length seq) = 0 then true
    else  
    match (String.length seq) with
    | 0 -> true
    | _ -> (is_nuc seq.[0]) && is_dna (String.sub seq 1 ((String.length seq)-1));
  in
  is_dna seq;;

let adn = "ACGAAATAAAACGTAA";;
let arn = "AKDJHFPZKD";;

print_endline(string_of_bool (is_seq_dna arn));;