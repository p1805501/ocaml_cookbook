let test_list = [1;2;3;4;5;6;7;8;9;10];;

let rec list_sum list =
  match list with
  | [] -> 0
  | h::t -> h + (list_sum t);;

print_int (list_sum test_list) ; print_endline("") 