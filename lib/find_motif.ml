
let rec find_motif ?(pos=0) motif sequence = (
  let lm = String.length motif in 
  let ls = String.length sequence in 
  if lm > ls then [] (* if the motif is longer than the sequence, it's not present*)
  else match ls with
  | x when x=lm -> if motif = sequence then pos :: [] else [] (* if the motif has the same length than the sequence, either it is position 0, or it is absent *)
  | _ -> if motif = (String.sub sequence 0 lm) then pos :: find_motif motif (String.sub sequence 1 (ls-1)) ~pos:(pos+1)
  else find_motif motif (String.sub sequence 1 (ls-1)) ~pos:(pos+1) (* otherwise, we make the recursive call*)
);;

let find_motif motif sequence = 
  let res = ref [] in 
  let length_motif = String.length motif in 
  for i = 0 to (String.length sequence - length_motif) do 
    if (String.sub sequence i length_motif) = motif 
      then res := i :: !res; done; 
  !res;;

(* less case *)

let rec find_motif ?(pos=0) motif sequence = (
  let lm = String.length motif in 
  let ls = String.length sequence in 
  if lm > ls 
    then [] (* if the motif is longer than the sequence, it's not present*)
    else 
      if motif = (String.sub sequence 0 lm) 
        then pos :: find_motif motif (String.sub sequence 1 (ls-1)) ~pos:(pos+1) 
        else find_motif motif (String.sub sequence 1 (ls-1)) ~pos:(pos+1) (* otherwise, we make the recursive call*)
);;

(* terminal recursivity *)
let rec print_list = function
| [] -> ()
| e::l -> print_int e ; print_string " " ; print_list l;;

let rec find_motif ?(pos=0) ?(acc=[]) motif sequence = (
  let lm = String.length motif in 
  let ls = String.length sequence in 
  if lm > ls 
    then acc 
    else 
      let pos_list = if motif = (String.sub sequence 0 lm) 
        then pos :: acc
        else acc
      in
    find_motif ~pos:(pos+1) ~acc:pos_list motif (String.sub sequence 1 (ls-1))
);;


(* se servir de pos au lien de string.sub a verifier mais normaement fonctionnel slt*)
let rec find_motif ?(pos=0) ?(acc=[]) motif sequence = (
  let lm = String.length motif in 
  let ls = String.length sequence - pos in 
  if lm > ls 
    then acc 
    else 
      let pos_list = if motif = (String.sub sequence pos lm) 
        then pos :: acc
        else acc
      in
    find_motif ~pos:(pos+1) ~acc:pos_list motif sequence 
);;


let rec isAt_aux ?(it=0) ?(pos=0) motif sequence = (
  if it = String.length motif
    then true
    else sequence.[pos+it] = motif.[it] && isAt_aux ~it:(it+1) ~pos:pos motif sequence  
);;

let rec isAt ?(pos=0) motif sequence = (
  if String.length motif > String.length sequence - pos
    then false
    else isAt_aux ~pos:pos motif sequence
);;


let rec find_motif ?(pos=0) ?(acc=[]) motif sequence = (
  let lm = String.length motif in 
  let ls = String.length sequence - pos in 
  if lm > ls 
    then acc 
    else 
      let pos_list = if isAt ~pos:pos motif sequence
        then pos :: acc
        else acc
      in
    find_motif ~pos:(pos+1) ~acc:pos_list motif sequence 
);;

let rec find_motif_aux ?(pos=0) ?(acc=[]) motif sequence = (
  let lm = String.length motif in 
  let ls = String.length sequence - pos in 
  if lm > ls 
    then acc 
    else 
      let pos_list = if isAt ~pos:pos motif sequence
        then pos :: acc
        else acc
      in
    find_motif ~pos:(pos+1) ~acc:pos_list motif sequence 
);;

let find_motif ?(pos=0) motif sequence = (
  find_motif ~pos:pos motif sequence
)

let rec print_list = function
| [] -> ()
| e::l -> print_int e ; print_string " " ; print_list l;;

(*print_string (string_of_bool (isAt "G" "GARIEL"));;*)
print_list (find_motif "GCG" "GCGCGATACGCGCGACGTACGTAGCGCG")