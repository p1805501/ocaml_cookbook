let sequence = "ACGAAATAAAACGTAA";;


(* 
let gc_content3 seq = 
  if String.length seq = 0 then failwith ("The sequence is empty")
  else (
    let rec gc_number seq = 
      match (String.length seq) with
      | 0 -> 0.
      | _ -> 
        let count =
        match seq.[0] with
        | 'C' | 'G' -> 1. 
        | 'A' | 'T' -> 0.
        | _ -> failwith ("This sequence is not DNA") 
        in
        count +. (gc_number (String.sub seq 1 ((String.length seq)-1)) )    
    in
    let number = gc_number seq in
    number /. (float_of_int (String.length seq))
  );;

gc_content sequence;;

let imperativ_gc_content seq =
  let count = ref 0. in 
  if String.length seq = 0 then failwith ("the sequence is empty")
  else 
    for i = 0 to String.length seq - 1 do
      count := 
        match Char.uppercase_ascii seq.[i] with
        | 'C' | 'G' -> !count +. 1.;
        | 'A' | 'T' -> !count +. 0.;
        | _ -> failwith ("This sequence is not DNA");
      done ;
    !count /. (float_of_int (String.length  seq));;

imperativ_gc_content sequence;;

let gc_content4 (Sequence seq) = 
  if String.length seq = 0 then failwith ("The sequence is empty")
  else (
    let rec gc_number seq = 
      match seq with
      | [] -> 0.
      | h::t -> 
        let count =
        match h with
        | 'C' | 'G' -> 1. 
        | 'A' | 'T' -> 0.
        | _ -> failwith ("This sequence is not DNA") 
        in
        count +. (gc_number t )    
    in
    let number = gc_number seq in
    number /. (float_of_int (String.length seq))
  );;

let gc_content_terminal_rec seq = 
  if String.length seq = 0 then failwith ("The sequence is empty") (* 1 *)
  else (
    let rec gc_number seq ?(acc=0.) = (* 2 *)
      match (String.length seq) with
      | 0 -> 0. (* 3 *)
      | _ -> 
        let count = (* 4 *)
        match seq.[0] with 
        | 'C' | 'G' -> 1. 
        | 'A' | 'T' -> 0. (* 5 *)
        | _ -> failwith ("This sequence is not DNA") (* 6 *)
        in
        count +. (gc_number (String.sub seq 1 ((String.length seq)-1)) ) (* 7 *)    
    in
    let number = gc_number seq in
    number /. (float_of_int (String.length seq)) (* 8 *)
  );; *)



let rec gc_number ?(acc=0.) seq = (* 2 *)
match (String.length seq) with
| 0 -> acc +. 0. (* 3 *)
| _ -> 
  let count = (* 4 *)
  match seq.[0] with 
  | 'C' | 'G' -> 1. 
  | 'A' | 'T' -> 0. (* 5 *)
  | _ -> failwith ("This sequence is not DNA") (* 6 *)
  in
  gc_number ~acc:(acc +. count) (String.sub seq 1 ((String.length seq)-1)) (* 7 *);;

gc_number ~acc:0. sequence ;;