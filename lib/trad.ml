#load "str.cma"

let rec print_list = function
| [] -> ()
| e::l -> print_string e ; print_string "\n" ; print_list l;;


let with_file path func = (
  let channel = open_in path in
  try
    let func_result = func channel in
    let () = close_in channel in 
    func_result
  with e -> let () = close_in channel in raise e
);;

let rec string_of_file ?(interline="\n") ?(already_read="") channel = (
  try
    let line = input_line channel in 
    string_of_file ~interline:"" ~already_read:(already_read ^ interline ^line) channel
  with End_of_file -> already_read
);;

let json2list path = (
  let jsonline = with_file path (string_of_file ~interline:"") in 
  let jsonline_without_bracket = Str.global_replace (Str.regexp "[ \"{}]") "" jsonline in 
  Str.split (Str.regexp ",") jsonline_without_bracket
);;

let rec add_kv_hashcodon ht l = (
  match l with
  | [] -> ();
  | hd::tl -> let kv = Str.split (Str.regexp ":") hd in
  Hashtbl.add ht (List.nth kv 0) ((List.nth kv 1).[0]) ;
  add_kv_hashcodon ht tl
);;

let hashcodon_of_list l = ( (* string, char hash table*)
  let codon_hash = Hashtbl.create 64 in 
  let () = add_kv_hashcodon codon_hash l in
  codon_hash
);;

module CodonDict = Map.Make(String) ;;

let rec mapcodon_of_list l = (
  match l with
  | [] -> CodonDict.empty
  | hd::tl -> let kv = Str.split (Str.regexp ":") hd in
      CodonDict.add (List.nth kv 0) ((List.nth kv 1).[0]) (mapcodon_of_list tl)
);;

let print_iter k v = (
  print_string k;
  print_string " ";
  print_char v;
  print_endline "";
);;

let codon_list = json2list "codon_table.json";;
let codon_hash = hashcodon_of_list codon_list;;
(*Hashtbl.iter print_iter codon_hash;;*)
let codon_map = mapcodon_of_list codon_list;;
(* CodonDict.iter print_iter codon_map;; *)

(*
let translation_codon codon dict = (
  match dict with
  | Hashtbl (a,b) -> Hashtbl.find dict codon
  | CodonDict -> CodonDict.find codon dict
  | _ -> failwith "Unsupported Data Structure for Codon Table"
);;

let translation_hash_codon codon ht = (
  Hashtbl.find ht codon
);;*)

let rec translation_hash_aux ?(acc="") ?(pos=0) ht sequence = (
  if pos == String.length sequence then
    acc
  else 
    let amino_acid = Hashtbl.find ht (String.sub sequence pos 3) in 
    translation_hash_aux ~acc:(acc^(Char.escaped amino_acid)) ~pos:(pos+3) ht sequence
);;

let translation_hash ht sequence = (
  if String.length sequence mod 3 != 0 then 
    failwith "No Coding Sequence (the length need to be a multiple of 3)"
  else translation_hash_aux ht sequence
);;

let rec translation_map_aux ?(acc="") ?(pos=0) table sequence = (
  if pos == String.length sequence then
    acc
  else 
    let amino_acid = CodonDict.find (String.sub sequence pos 3) table in 
    translation_map_aux ~acc:(acc^(Char.escaped amino_acid)) ~pos:(pos+3) table sequence
);;

let translation_map table sequence = (
  if String.length sequence mod 3 != 0 then 
    failwith "No Coding Sequence (the length need to be a multiple of 3)"
  else translation_map_aux table sequence
);;



print_endline (translation_map codon_map "AUGGGAUAA");;

(*
let rec traduction_codon codon = (
  let l = (String.length codon) in 
  if l mod 3 != 0 then raise (Invalid_argument "Not a Codon")
  else match codon with
  | "AUG" -> 'M'
  | "UUU" -> 'F'
  | "UGA" -> '*'
  | _ -> raise (Invalid_argument "Not a Codon")
);;


class Dictionary =
  object (self)
    val mutable keyval = ([] : (string * string) list)
    method add (key * valu ) =
      match keyval with   
      | [] -> 
      | (k, v)::tl with k=key -> raise (Invalid_argument "Key already in the dict") *)
