def gc_content (seq):
  count = 0
  if len(seq) = 0 :
    raise ValueError ("the string is empty")
  else :
    for i in seq :
      if i.upper() in ["C","G"] :
        count += 1
      elif i.upper() not in ["A","T"] :
        raise ValueError ("the sequence is not DNA")
  return (count/(len (seq)))


sequence = "ACGAdATAAAACGTAA"
print(gc_content (sequence))