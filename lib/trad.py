dna = "ATGGGAUGUUGA"

with open("codon_table.json") as table:
    content = table.read()
    content = content.replace('\n','')
    content = content.replace('{','')
    content = content.replace('}','')
    content = content.replace(' ','')
    content = content.replace('"','')
    content = content.split(",")
    codontable = dict()
    for kv in content:
        k,v = kv.split(':')
        codontable[k]=v

print(codontable)