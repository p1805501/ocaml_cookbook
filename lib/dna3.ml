type nucleotide = A | C | G | T ;;

type dna = Sequence of nucleotide list;;

let nuc_of_char character = 
  match (Char.uppercase_ascii character) with
  | 'A' -> A
  | 'C' -> C
  | 'G' -> G
  | 'T' -> T
  | _ -> failwith ("This sequence is not DNA");;
  
let dna_of_string seq = 
  let rec nuclist_of_string sequence =
    match (String.length sequence) with
    | 0 -> []
    | _ -> nuc_of_char (sequence.[0]) :: nuclist_of_string (String.sub sequence 1 ((String.length sequence)-1))
  in
  let nuc_list = nuclist_of_string seq in
  Sequence nuc_list;;

let dna_of_string2 seq = 
  let rec nuclist_of_string ?(acc=[]) sequence =
    match String.length sequence with
    | 0 -> acc
    | _ -> nuclist_of_string ~acc:((nuc_of_char (sequence.[0])) :: acc) (String.sub sequence 1 ((String.length sequence)-1))
  in
  let nuc_list = List.rev (nuclist_of_string seq) in
  Sequence nuc_list;;
    
let nuc_of_char2 character = 
  match (Char.uppercase_ascii character) with
  | 'A' -> Some A
  | 'C' -> Some C
  | 'G' -> Some G
  | 'T' -> Some T
  | _ -> None 

let dna_of_string2 seq = 
  let rec nuclist_of_string sequence =
    match (String.length sequence) with
    | 0 -> []
    | _ -> 
      let el1 = match (nuc_of_char2 (sequence.[0])) with
      | Some x -> x
      | None -> failwith ("This sequence is not DNA") 
      in
      el1 :: nuclist_of_string (String.sub sequence 1 ((String.length sequence)-1))
  in
  let nuc_list = nuclist_of_string seq in
  Sequence nuc_list;;

let reverse_complement (Sequence dna) =
  let rec complement dna =
    match dna with
    | [] -> []
    | h::t -> match h with 
      | C -> G :: (complement t)
      | G -> C :: (complement t)
      | A -> T :: (complement t)
      | T -> A :: (complement t)
  in
  Sequence (complement dna);;

let adn = "ACGAAATAAAACGTAA";;

let arn = "ACfTAAAACGTAA";;
let dna_sequence = dna_of_string adn;;


reverse_complement dna_sequence;;


let gc_content3 (Sequence seq) = 
  if List.length seq = 0 then failwith ("The sequence is empty")
  else (
    let rec gc_number (Sequence seq) = 
      match seq with
      | [] -> 0.
      | h::t -> 
        let count =
        match h with
        | C | G -> 1. 
        | A | T -> 0.
        in
        count +. (gc_number (Sequence t))    
    in
    let number = gc_number (Sequence seq) in
    number /. (float_of_int (List.length seq))
  );;

let gc_content3 (Sequence seq) = 
  if List.length seq = 0 then failwith ("The sequence is empty")
  else (
    let rec gc_number (Sequence seq) = 
      match seq with
      | [] -> 0.
      | h::t -> 
        let count =
        match h with
        | C | G -> 1. 
        | A | T -> 0.
        in
        count +. (gc_number (Sequence t))    
    in
    let number_gc = gc_number (Sequence seq) in
    number_gc /. (float_of_int (List.length seq))
  );;

let rec string_of_dna ?(acc="") (Sequence seq) =
  match seq with
  | [] -> acc
  | h::t -> let character = 
    match h with 
    | A -> "A"
    | C -> "C"
    | G -> "G"
    | T -> "T"
    in string_of_dna ~acc:(acc^character) (Sequence t);;

let print_dna (Sequence seq) =
  let dna_string = string_of_dna ~acc:"" (Sequence seq) in
  print_endline dna_string;;

print_dna dna_sequence;;

let reverse_complement (Sequence dna) =
  let rec complement ?(acc=[]) dna_seq =
    match dna_seq with
    | [] -> acc
    | h::t -> let nucleotide =
      match h with 
      | C -> G 
      | G -> C
      | A -> T 
      | T -> A
    in
    complement ~acc:(nucleotide::acc) t
  in
  Sequence (List.rev (complement dna));;