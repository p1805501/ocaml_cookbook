let rl_string ?(interline="\n") path = (
  let f = open_in path in  
  let rec rl ?(already_read="") f = (
    try
      let line = input_line f in 
      if already_read = "" then rl ~already_read:(line) f 
      else rl ~already_read:(already_read ^ interline ^ line) f
    with End_of_file -> let () = close_in f in already_read
  ) in 
  rl f
);;

let rl_list path = (
  let f = open_in path in  
  let rec rl ?(already_read=[]) f = (
    try
      let line = input_line f in 
      rl ~already_read:(line::already_read) f
    with End_of_file -> let () = close_in f in List.rev already_read
  ) in 
  rl f
);;

let rec print_list = function
| [] -> ()
| e::l -> print_endline e ; print_list l;;

let with_file path func = (
  let channel = open_in path in
  try
    let func_result = func channel in
    let () = close_in channel in 
    func_result
  with e -> let () = close_in channel in raise e
);;

let rec list_of_file ?(already_read=[]) channel = (
  try
    let line = input_line channel in 
    list_of_file ~already_read:(line::already_read) channel
  with End_of_file ->   List.rev already_read
);;

print_list (with_file "histoire.txt" list_of_file)